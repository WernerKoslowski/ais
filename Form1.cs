﻿using Renci.SshNet;
using System;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization;
using System.Windows.Forms.DataVisualization.Charting;
using WinSCP;

namespace UDP_Client
{

    struct AppConfig_s
    {
        public uint ActiveSensors;
        public String[] SensorIPs;
        public int SensorPort;
        public int LocalPort;
        public int PlotCounter;
        public bool SaveStreams;
        public bool SaveStreamsActive;
        public bool SaveStreamsLocal;
        public int SaveStreamsCount;
        public CheckBox CheckedButton;
        public bool Active;
        public bool MeasurementsActive;
        public bool HeaderChanged;

        public void SetDefault( )
        {
            this.SensorIPs              = new string[2];
            this.Active                 = false;
            this.ActiveSensors          = 0;
            this.SensorIPs[0]           = "192.168.128.1";
            this.SensorIPs[1]           = "192.168.129.1";
            this.SensorPort             = 61231;
            this.LocalPort              = 61231;
            this.PlotCounter            = 0;
            this.SaveStreams            = false;
            this.SaveStreamsActive      = false;
            this.SaveStreamsLocal       = true;
            this.SaveStreamsCount       = 0;
            this.CheckedButton          = null;
            this.HeaderChanged          = true;
            this.MeasurementsActive     = false;
        }
       
    }



    struct FFTConfig_s
    {
        public int MinFrequency;
        public int MinFrequencyIndex;
        public int MaxFrequency;
        public int MaxFrequencyIndex;
        public int DataCount;
        public int ColorCounter;
        public double SampleFrequency;
        public double SampleDecimation;
        public double FrequencyFactor;
        public bool ConfigChanged;

        public void SetDefault()
        {
            // (double)119.2093, (int)293, (int)377
            this.MinFrequency       = 35000;
            this.MinFrequencyIndex  = 293;
            this.MaxFrequency       = 45000;
            this.MaxFrequencyIndex  = 377;
            this.DataCount          = MaxFrequencyIndex - MinFrequencyIndex + 1;
            this.ColorCounter       = 0;
            this.SampleFrequency    = 125 * Math.Pow(10, 6); // [Hz] --> 125 MHz 
            this.SampleDecimation   = 64;
            this.FrequencyFactor    = (SampleFrequency / SampleDecimation) / (Math.Pow(2, 14));
            this.ConfigChanged        = false;
        }
    }



    struct ChartConfig_s
    {
        public double YMax;
        public double YMin;
        public double XMax;
        public double XMin;
        public UInt32 XInc;
        public UInt32 YInc;
        public String XTitle;
        public String YTitle;

        public void SetBasics(String _XTitle, String _YTitle, double _XMin=0, double _XMax=16000, double _YMin=-10000, double _YMax=10000 )
        {
            XMin = _XMin;
            XMax = _XMax;
            YMin = _YMin;
            YMax = _YMax;
            XInc = (UInt32)(XMax-XMin)/4;
            YInc = (UInt32)(YMax-YMin)/10;
            XTitle = _XTitle;
            YTitle = _YTitle;
        }

        public void SetValues(double _XMin, double _XMax, double _YMin, double _YMax)
        {
            XMin = _XMin;
            XMax = _XMax;
            YMin = _YMin;
            YMax = _YMax;
            XInc = 1000;
            YInc = 250;
        }

        public void SetFeature()
        {
        }

        public int GetActive()
        {
            // 0 - ADC, 1 - FFT, 2 - Features
            return (0);
        }


    }


    


    public partial class Form1 : Form
    {
        internal static Form1 FM1 = null;
        internal static Form2 FM2 = null;
        internal static Form3 FM3 = null;

        private AutoResetEvent FillDataThreadSignal = new AutoResetEvent(false);
        private AutoResetEvent ProgRPThreadSignal = new AutoResetEvent(false);
        private AutoResetEvent UDP_ThreadSignal = new AutoResetEvent(false);

        Thread UDP_Thread = null;
        Thread FillDataThread = null;
        Thread ProgRPThread = null;
        IPAddress[] HostIPs;


        // UDP variables
        IPEndPoint[] EndPoint = new IPEndPoint[2] { null, null };
        IPEndPoint EndPointLocal = null;
        UdpClient UDP_Socket = null;
        bool UDPThreadFunctionRun = false;

        int PlotCounterOld = 0;

        // Readonly variables for text / paths
        readonly String ProgDirRemote = "/root/iic";
        readonly String ProgName = "iic";
        readonly String[] LearnObjects = { "nothing", "Wall", "Ceiling","HiFi board", "Human" };
        readonly int[] WindowWidths = { 1024, 2048, 4096, 8192 };
        readonly String[] YLabelsCharts = { "decimal Values", "Amplitude sqrt(re² +im²)" };
        readonly String[] XLabelsCharts = { "Samples [-]", "Frequency [Hz]" };

        String LibDirRemote = "";
        String ProgDirLocal = "";
        String WorkDirLocal = "";
        String SaveDirLocal = "";
        String ActualSaveFile = "";

        // File streams for local saves
        private FileStream Fs = null;


        UInt16[] XValuesAdc = { };
        UInt16[] XValuesFft = { };

        AppConfig_s AppConfig = new AppConfig_s();
        FFTConfig_s FFTConfig = new FFTConfig_s();
        ChartConfig_s ChartConfigAdc = new ChartConfig_s();
        ChartConfig_s ChartConfigFft = new ChartConfig_s();


        // Data information / buffer
        private Byte[]      UdpRxBuffer;                // general receive Buffer for UDP
        private float[]     HeaderInfo      = { };
        private Int16[]     AdcDataBuffer   = { };
        private UInt16[]    FftDataBuffer   = { };
        private float[]     FeatureBuffer   = { };
        private int         ActiveDataType;
        private byte[]      FileWriteBuffer;
        private String TmpString;
        private String Tmp;

        // start up / initialize
        public Form1()
        {
            InitializeComponent();
        }


        // start up program
        private void Form1_Load(object sender, EventArgs e)
        {
            // restore standard values
            // #####################################
            AppConfig.SetDefault();
            FFTConfig.SetDefault();
            // #####################################
            // end

            FM1 = this;

            // set all necessary directories
            SetWorkingDirectories(Directory.GetCurrentDirectory());
            

            // set IP-addresses
            TextBoxRemoteIP1.Text = AppConfig.SensorIPs[0];
            TextBoxRemoteIP2.Text = AppConfig.SensorIPs[1];
            TextBoxRemotePort1.Text = AppConfig.SensorPort.ToString();


            // fill the ComboBox for Window widths used for FFT
            CbWindowWith.Items.Clear();
            for(int ix = 0; ix < WindowWidths.Length; ix++)
            {
                CbWindowWith.Items.Add(WindowWidths[ix].ToString());
            }
            CbWindowWith.SelectedIndex = WindowWidths.Length - 1;



            CbLearnObject.Items.Clear();
            for(int ix=0; ix<LearnObjects.Length; ix++)
            {
                CbLearnObject.Items.Add(LearnObjects[ix]);
            }
            CbLearnObject.SelectedIndex = 1;

            LabelRPSWVersion.Text = "VX.YY";
            LabelGUISWVersion.Text = "V0.22";

            CheckSensor();

            // prepare some buttons etc. pp.
            ButtonAdcAutoWindowing.BackColor = Color.Transparent;
            TbSecondsToMeasure.Text = 1.ToString("0");
            CbADC.Checked = true;
            CbFFT.Checked = true;

            // create threads for background activity
            ProgRPThread = new Thread(new ThreadStart(ProgRPThreadFunction));
            FillDataThread = new Thread(new ThreadStart(FillDataThreadFunction));
            UDP_Thread = new Thread(new ThreadStart(UDPThreadFunction));

            // configure all threads
            FillDataThread.IsBackground = true;
            FillDataThread.Priority = ThreadPriority.Normal;
            UDP_Thread.IsBackground = true;
            UDP_Thread.Priority = ThreadPriority.AboveNormal;
            ProgRPThread.IsBackground = true;
            ProgRPThread.Priority = ThreadPriority.BelowNormal;

            

            FillDataThread.Start();
            UDP_Thread.Start();
            ProgRPThread.Start();
        }



        // end / exit program
        private void Form1_Close(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (UDP_Socket != null)
                {
                    UDP_Socket.Close();
                }
            }
            catch{}
           
        }



        private void ButtonCheckSensor_Click(object sender, EventArgs e)
        {
            SetStatus("Check for sensors!");
            CheckSensor();
            if(AppConfig.ActiveSensors == 1)
            {
                SetStatus("Sensor 1 active!");
            }
            else if(AppConfig.ActiveSensors == 2)
            {
                SetStatus("Sensor 2 active!");
            }
            else if(AppConfig.ActiveSensors == 3)
            {
                SetStatus("Sensor 1 and 2 active!");
            }
            else
            {
                SetStatus("No active sensor!");
            }
        }



        private void ButtonStartX_Click(object sender, EventArgs e)
        {
            CheckBox ButtonIn = (CheckBox)sender;
            String ButtonName = ButtonIn.Name;
            bool Checked = ButtonIn.Checked;
            int ActiveMeasurements = 0;
            String Cmd;

            AppConfig.MeasurementsActive = Checked;
            CbADC.Enabled = !Checked; CbFFT.Enabled = !Checked; CbFeature.Enabled = !Checked;
            TbSecondsToMeasure.Enabled = !Checked;

            ButtonStartLiveMeasure.Enabled = !Checked;
            ButtonStartLocalLogging.Enabled = !Checked;
            ButtonStartRemoteLogging.Enabled = !Checked;
            BoxChart.Enabled = Checked;
            
            ButtonIn.BackColor = (Checked) ? (Color.LightGreen) : (Color.Transparent);

            ActiveMeasurements = ((CbADC.Checked) ? (0x01) : (0x00)) | ((CbFFT.Checked) ? (0x02) : (0x00)) | ((CbFeature.Checked) ? (0x04) : (0x00));
            
            AppConfig.CheckedButton = ButtonIn;
            AppConfig.PlotCounter = 0;
            LabelPlotCount.Text = "0";

            switch (ButtonName)
            {
                case "ButtonStartLiveMeasure":
                    if(Checked)
                    {
                        ChartConfigAdc.SetBasics(XLabelsCharts[0], YLabelsCharts[0]);
                        ChartConfigFft.SetBasics(XLabelsCharts[1], YLabelsCharts[1]);
                        ChartConfigFft.SetValues(35000, 45000, 0, 1000);
                        SetChart(ref ChartConfigAdc, 0);
                        SetChart(ref ChartConfigFft, 1);
                    }
                    Cmd = "-s " + ((Checked)?("1"):("0")) + " -1 " + ActiveMeasurements.ToString("0") ;      // start or stop stream
                    UDPSendData(Cmd);
                    break;

                case "ButtonStartLocalLogging":
                    AppConfig.SaveStreams = Checked;
                    AppConfig.CheckedButton = ButtonIn;
                    ActualSaveFile = SaveDirLocal + "\\" + CbLearnObject.SelectedItem.ToString() + ".bin";
                    Cmd = "-s 0 0 0";

                    if ( AppConfig.SaveStreams)
                    {
                        if(Int32.TryParse(TbSecondsToMeasure.Text, out int Number))
                        {
                            if(Number > 0)
                            {
                                AppConfig.SaveStreamsCount = Number * 8;
                                Cmd = "-s 1 -1 " + ActiveMeasurements.ToString("0");
                                if (Fs != null)
                                {
                                    Fs.Close(); Fs.Dispose(); Fs = null;
                                }
                                Fs = File.Open(ActualSaveFile, FileMode.Append);
                                FileWriteBuffer = new byte[Number * 8 * (int)Math.Pow(2,16)];
                            }
                            else
                            {
                                ButtonIn.Checked = !Checked;
                            }
                        }
                        else
                        {
                            TbSecondsToMeasure.Text = (0).ToString();
                            ButtonIn.Checked = !Checked;
                        }
                    }
                    else
                    {
                        if(Fs != null)
                        {
                            Fs.Close(); Fs.Dispose(); Fs = null;
                        }
                    }
                    UDPSendData(Cmd);
                    break;

                case "ButtonStartRemoteLogging":
                    if (Checked)
                    {
                        Cmd = "-l " + TbSecondsToMeasure.Text + " " + ActiveMeasurements.ToString("0");
                        UDPSendData(Cmd);
                        ButtonIn.Checked = !Checked;
                    }
                    break;

                case "ButtonStartConverter":
                    if(FM3 == null && Checked)
                    {
                        FM3 = new Form3();
                        FM3.ShowDialog();
                        FM3 = null;
                        ButtonIn.Checked = !Checked;
                    }
                    break;

                default:
                    break;
            }

            ButtonIn.Enabled = true;

        }



        private void ButtonStartSensor_Click(object sender, EventArgs e)
        {
            String Cmd = "pkill " + ProgName;

            SetStatus("stop sensor...");
            SShSendData(Cmd, 0);
            SetStatus("start sensor...");
            Cmd = "./" + ProgName;
            SShSendData(Cmd,0);
        }



        private void ButtonProgramRP_Click(object sender, EventArgs e)
        {
           
            if( File.Exists(ProgDirLocal + "\\iic") )
            {
                if(File.Exists(ProgDirLocal + "\\cronjob"))
                {
                    ProgRPThreadSignal.Set();
                }
            }
            
            
        }



        private void ButtonStartUDPClient_CheckedChanged(object sender, EventArgs e)
        {
            
            CheckBox Cb = (CheckBox)sender;
            bool Checked = Cb.Checked;
            String IP1 = AppConfig.SensorIPs[0];
            String IP2 = AppConfig.SensorIPs[1];
            int Port = AppConfig.SensorPort;

            Cb.BackColor = (Checked) ? (Color.LightGreen) : (Color.Transparent);

            // Clientconfiguration for UDP
            EndPoint[0] = new IPEndPoint(IPAddress.Parse(IP1), (int)Port);
            EndPoint[1] = new IPEndPoint(IPAddress.Parse(IP2), (int)Port);
            EndPointLocal = new IPEndPoint(IPAddress.Any, (int)Port);

            BoxConfig.Enabled = !Checked;
            BoxRxTx.Enabled = Checked;
            BoxChart.Enabled = Checked;

            if (UDP_Socket != null){
                UDP_Socket.Close();
            }

            AppConfig.Active = Checked;
            ButtonStartClient.Text = (Checked)?( "stop client"):("start client");

            if (Checked)
            {
                UDP_Socket = new UdpClient(EndPointLocal);
                UDPSendData("-s 0 0 0");
                UDPSendData("-r " + DateTime.Now.ToString("yyyy-MM-dd H:mm:ss"));
            }
            else
            {
                LabelRPSWVersion.Text = "VX.YYz";
            }

            StartUdpThread(Checked);
        }



        private void ButtonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }



        // ##########################################
        // Thread functions
        // ##########################################

        public void StartUdpThread(bool Start = true)
        {
            UDPThreadFunctionRun = Start;
            if (Start)
            {
                UDP_ThreadSignal.Set();
            }
        }



        public void UDPThreadFunction()
        {

            while (true)
            {
                if (!UDPThreadFunctionRun)
                {
                    // wait for Signal!
                    UDP_ThreadSignal.WaitOne();
                }
                try
                {
                    // go into blocked mode and wait for header data
                    UdpRxBuffer = UDP_Socket.Receive(ref EndPointLocal);

                    using (BinaryReader Br = new BinaryReader(new MemoryStream(UdpRxBuffer)))
                    {
                        // read out header length
                        int HeaderLength = (int)Br.ReadSingle();
                        int AdcLength = 0;
                        int FftLength = 0;
                        int FeatureLength = 0;
                        int ix = 0;

                        HeaderInfo = new float[HeaderLength/4];
                        HeaderInfo[ix++] = HeaderLength;

                        // read out the header Information
                        while (ix < (HeaderLength / 4) )
                        {
                            HeaderInfo[ix++] = Br.ReadSingle();
                        }

                        ActiveDataType = Convert.ToInt32(HeaderInfo[5]);

                        // which kind of data was received?
                        if ((ActiveDataType & 1) != 0)                                // ADC Data was received
                        {
                            // ADC active
                            ix = 0;
                            AdcLength = (int)(HeaderInfo[1]);
                            AdcDataBuffer = new Int16[AdcLength / 2];
                            while( ix < AdcLength / 2)
                            {
                                AdcDataBuffer[ix++] = Br.ReadInt16();
                            }
                            
                            if (AdcDataBuffer.Length != XValuesAdc.Length)
                            {
                                XValuesAdc = new UInt16[AdcDataBuffer.Length];
                                for (UInt16 ixx = 0; ixx < XValuesAdc.Length; ixx++)
                                {
                                    XValuesAdc[ixx] = ixx;
                                }
                            }
                        }
                        
                        if ((ActiveDataType & 2) != 0)                           // FFT data was received
                        {
                            ix = 0;
                            FftLength = (int) (Math.Round( (HeaderInfo[2] ), 0));
                            FftDataBuffer = new UInt16[FftLength / 2];
                            

                            while ( ix < ( FftLength / 2) )
                            {
                                FftDataBuffer[ix++] = Br.ReadUInt16();
                            }
                            
                            if (FftDataBuffer.Length != XValuesFft.Length)
                            {
                                // frequency step between samples in FFT
                                float m = HeaderInfo[6];
                                float n = HeaderInfo[14] * m;
                                XValuesFft = new UInt16[FftDataBuffer.Length];
                                for (int ixx = 0; ixx < XValuesFft.Length; ixx++)
                                {
                                    XValuesFft[ixx] = (UInt16)(n + m * (float)ixx);
                                }
                            }
                        }

                        if ((ActiveDataType & 4) != 0)                           // Fature data was received
                        {
                            Tmp = "";
                            ix = 0;
                            FeatureLength = (int)(HeaderInfo[3]);
                            FeatureBuffer = new float[FeatureLength / 4];
                            while(ix < (FeatureLength / 4 ))
                            {
                                FeatureBuffer[ix++] = Br.ReadSingle();
                            }

                            for (int ixx = 0; ixx < (FeatureBuffer.Length); ixx++)
                            {
                                Tmp += "F" + (ixx + 1).ToString("#0: ") + FeatureBuffer[ixx].ToString("00000.0") + ((ixx == 4) ? (Environment.NewLine) : ("\t"));
                            }
                        }

                        // now refresh the screen with data, if desired
                        if (AppConfig.MeasurementsActive)
                        {
                            // start the thred for filling data into the Form
                            FillDataThreadSignal.Set();
                        }
                    } // end using binary reader RxBuffer

                }
                catch { }
            }
        }



        public void FillDataThreadFunction()
        {
            DateTime ToDay = DateTime.Now.Date;
            DateTime Dt;

            ToDay = ToDay.AddHours(1);

            for (; ; )
            {
                // wait for signal from UDP thread!
                FillDataThreadSignal.WaitOne();

                Dt = ToDay.AddSeconds(HeaderInfo[18]);

                MethodInvoker QuestionDelegate = delegate
                {
                    if (((ActiveDataType & 0x01) != 0))
                    {
                        if (XValuesAdc.Length == AdcDataBuffer.Length)
                        {
                            ChartAdc.Series[0].Points.DataBindXY(XValuesAdc, AdcDataBuffer);
                        }
                    }

                    if ((ActiveDataType & 0x02) != 0)
                    {
                        if (XValuesFft.Length == FftDataBuffer.Length)
                        {
                            ChartFFT.Series[0].Points.DataBindXY(XValuesFft, FftDataBuffer);
                        }
                    }

                    if ((ActiveDataType & 0x04) != 0)
                    {
                        TbFeatures.Text = Tmp;
                    }

                    
                    TmpString = "Bytes: Head[" + (HeaderInfo[0]).ToString(" ##0 ") + "]  |  Data[" + (HeaderInfo[1] + HeaderInfo[2] + HeaderInfo[3]).ToString(" ####0 ") + "]" +
                        "  |  All[" + (HeaderInfo[0] + HeaderInfo[1] + HeaderInfo[2] + HeaderInfo[3]).ToString(" ####0 ") + "]" + " Timestamp: " + Dt.ToString("yyyy-MM-dd HH:mm:ss");
                    TbClass.Text = HeaderInfo[4].ToString("0");
                    TbDistance.Text = HeaderInfo[12].ToString("0.000 m");
                    LabelPlotCount.Text = (++AppConfig.PlotCounter).ToString();
                    LabelRxPayload.Text = TmpString;
                    

                    if (AppConfig.SaveStreams)
                    {
                        if (Fs != null)
                        {
                            // write all bytes to file
                            Fs.Write( UdpRxBuffer, 0, UdpRxBuffer.Length);
                        }

                        if ((--AppConfig.SaveStreamsCount) <= 0)
                        {
                            AppConfig.SaveStreams = false;
                            AppConfig.CheckedButton.Checked = !AppConfig.CheckedButton.Checked;
                        }
                    }

                };
                Invoke(QuestionDelegate);

            }
        }



        public void ProgRPThreadFunction()
        {
            // init here

            while(true)
            {
                // stay here in blocked mode, waiting for signal to run!
                ProgRPThreadSignal.WaitOne();

                // signal received, now run!
                uint ret = PingSensor();
                WinSCP.Session SCP_Client = new WinSCP.Session();
                WinSCP.SessionOptions ScpSensorOptions;// = new SessionOptions();


                for (int ix = 0; ix < 2; ix++)
                {
                    if ((ret & (1 << ix)) != 0 )
                    {
                        ScpSensorOptions = new WinSCP.SessionOptions { Protocol = Protocol.Scp, HostName = AppConfig.SensorIPs[ix], UserName = "root", Password = "root", GiveUpSecurityAndAcceptAnySshHostKey = true, PortNumber = 22, };
                        SetStatus("programming sensor " + (ix + 1).ToString(), true, false, 10000);
                        SShSendData("pkill " + ProgName, ix);
                        SCP_Client.Open(ScpSensorOptions);
                        if (!SCP_Client.FileExists(ProgDirRemote))
                        {
                            SCP_Client.CreateDirectory(ProgDirRemote);
                        }
                        SCP_Client.SynchronizeDirectories(SynchronizationMode.Remote, ProgDirLocal, ProgDirRemote, true, true, SynchronizationCriteria.Either);
                        SShSendData("chmod -R 777 " + ProgDirRemote);
                        SShSendData("systemctl enable cron.service");
                        SShSendData("crontab /root/iic/cronjob", ix);
                        SShSendData("./" + ProgName, ix);

                        if (!SCP_Client.FileExists(ProgDirRemote + "/" + ProgName))
                        {
                            SetStatus("failure sensor " + (ix + 1).ToString(), true, true);
                        }
                        else
                        {
                            SetStatus("successfully sensor programmed: " + (ix + 1).ToString());
                        }
                        SCP_Client.Close();
                        Thread.Sleep(500);
                    }
                }

            }
        }



        public void UDPSendData(string tx_data, uint SensorNumber = 0)
        {
            uint ix = SensorNumber % (uint)EndPoint.Length;
            Byte[] sendBytes = Encoding.ASCII.GetBytes(tx_data);

            UDP_Socket.Send(sendBytes, sendBytes.Length, EndPoint[ix]);
        }



        public int SShSendData( string tx_data, int SensorNumber=0 )
        {
            int ix = (int)( SensorNumber % AppConfig.SensorIPs.Length );
            SshClient RP_Client;

            if ( (AppConfig.ActiveSensors & ( 1 << ix )) != 0)
            {
                RP_Client = new SshClient(AppConfig.SensorIPs[ix], 22, "root", "root");
                tx_data = "(cd " + ProgDirRemote + "; " + tx_data + ")";
                RP_Client.Connect();
                RP_Client.RunCommand(tx_data);
                RP_Client.Disconnect();
            }

            return 0;
        }



        public uint PingSensor()
        {
            Ping RP_Ping = new Ping();
            IPStatus stat;
            uint ret = 0 ;

            try
            {
                stat = RP_Ping.Send(AppConfig.SensorIPs[0], 50, Encoding.ASCII.GetBytes("0")).Status;
                ret |= (stat == IPStatus.Success) ? ((uint)1) : ((uint)0);

                stat = RP_Ping.Send(AppConfig.SensorIPs[1], 50, Encoding.ASCII.GetBytes("0")).Status;
                ret |= (stat == IPStatus.Success) ? ((uint)2) : ((uint)0);

                AppConfig.ActiveSensors = ret;
            }
            catch { }
            

            return (ret);
        }



        private void LabelWorkingDir_Click(object sender, EventArgs e)
        {
            //folderBrowserDialog1.RootFolder = Environment.SpecialFolder.DesktopDirectory;
            folderBrowserDialog1.SelectedPath = Directory.GetCurrentDirectory();
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                SetWorkingDirectories(folderBrowserDialog1.SelectedPath);
            }
        }



        private void LabelOpenWorkingDir_Click(object sender, EventArgs e)
        {
            Process.Start(WorkDirLocal);
        }



        private void LabelOpenProgramDir_Click(object sender, EventArgs e)
        {
            Process.Start(ProgDirLocal);
        }



        private void LabelOpenDataDir_Click(object sender, EventArgs e)
        {
            Process.Start(SaveDirLocal);
        }



        private void SetWorkingDirectories(String data_in)
        {
            LibDirRemote = ProgDirRemote + "/lib";

            WorkDirLocal = data_in;
            ProgDirLocal = WorkDirLocal + "\\proc";
            SaveDirLocal = WorkDirLocal + "\\save";
            Directory.CreateDirectory(ProgDirLocal);
            Directory.CreateDirectory(SaveDirLocal);
            LabelWorkDir.Text = WorkDirLocal;
            LabelProgDir.Text = ProgDirLocal;
            LabelSaveDir.Text = SaveDirLocal;
        }

        

        private void SetStatus(String data_in, bool action=true, bool ErrorIn=false, int mSec=3000)
        {
            MethodInvoker QuestionDelegate = delegate
            {
                LableStatusGlobal.Text = "info: " + data_in;
                if (action)
                {
                    LableStatusGlobal.ForeColor = (ErrorIn)?(Color.Red): (Color.Blue);
                    T1.Stop();
                    T1.Enabled = true;
                    T1.Interval = mSec;
                    T1.Start();
                }
                   
                else
                {
                    LableStatusGlobal.ForeColor = Color.Black;
                }

            };
            Invoke(QuestionDelegate);
        }



        // Timer function(s)
        private void T1_Tick(object sender, EventArgs e)
        {
            SetStatus("idle", false);
            T1.Stop();
            T1.Enabled = false;
        }



        // Chart / Series function(s)
        /*
         * Test
         * 
         */
        private void SetChart( ref ChartConfig_s Cc, int ChartNumber=0)
        {
            ChartArea CA;


            if(ChartNumber == 0)
            {
                CA = ChartAdc.ChartAreas[0];
            }
            else
            {
                CA = ChartFFT.ChartAreas[0];
            }

            // X-Axis
            CA.IsSameFontSizeForAllAxes = true;
            CA.AxisX.TitleForeColor = Color.Green;
            CA.AxisX.MajorGrid.LineColor = Color.SlateGray;
            CA.AxisX.MinorGrid.Enabled = true;
            CA.AxisX.MinorGrid.LineColor = Color.LightGray;
            CA.AxisX.Title = Cc.XTitle;
            CA.AxisX.Minimum = Cc.XMin;
            CA.AxisX.Maximum = Cc.XMax;
            CA.AxisX.Interval = Cc.XInc;
            CA.AxisX.MajorGrid.Interval = Cc.XInc;
            CA.AxisX.MinorGrid.Interval = Cc.XInc / 2;

            // Y-Axis
            CA.AxisY.TitleForeColor = Color.Green;
            CA.AxisY.MajorGrid.LineColor = Color.SlateGray;
            CA.AxisY.MinorGrid.Enabled = true;
            CA.AxisY.MinorGrid.LineColor = Color.LightGray;
            CA.AxisY.Title = Cc.YTitle;
            CA.AxisY.Minimum = Cc.YMin;
            CA.AxisY.Maximum = Cc.YMax;
            CA.AxisY.IsStartedFromZero = true;
            CA.AxisY.Interval = Cc.YInc;
            CA.AxisY.MajorGrid.Interval = Cc.YInc;
            CA.AxisY.MinorGrid.Interval = Cc.YInc / 2;

            
        }



        private void TimerRxSpeed_Tick(object sender, EventArgs e)
        {
            LabelPlotSpeed.Text = Convert.ToString( Math.Abs(AppConfig.PlotCounter - PlotCounterOld)/ (TimerRxSpeed.Interval/1000) );
            PlotCounterOld = AppConfig.PlotCounter;
        }



        private void LabelSaveDir_Click(object sender, EventArgs e)
        {
            Process.Start(SaveDirLocal);
        }



        private void LabelVersionInfo_Click(object sender, EventArgs e)
        {
            Process.Start(ProgDirLocal + "\\src\\iic.c");
        }



        private void TextBoxRemoteIP1_TextChanged(object sender, EventArgs e)
        {
            AppConfig.SensorIPs[0] = TextBoxRemoteIP1.Text;
        }



        private void TextBoxRemoteIP2_TextChanged(object sender, EventArgs e)
        {
            AppConfig.SensorIPs[1] = TextBoxRemoteIP2.Text;
        }



        private void TextBoxRemotePort1_TextChanged(object sender, EventArgs e)
        {
            AppConfig.SensorPort = Int32.Parse(TextBoxRemotePort1.Text);
        }



        private void ButtonSendCmd_Click(object sender, EventArgs e)
        {
            UDPSendData(TbSendCmd.Text);
        }



        private void CheckSensor()
        {
            bool active;
            HostIPs = Dns.GetHostAddresses(Environment.MachineName);
            String HostIP = "";
            
            AppConfig.ActiveSensors = PingSensor();

            TextBoxLocalIP1.Text = "127.0.0.1";
            TextBoxLocalIP2.Text = "127.0.0.1";

            active = (AppConfig.ActiveSensors > 0);

            for (int ix = 0; ix < HostIPs.Length; ix++)
            {
                HostIP = HostIPs[ix].ToString();
                if (HostIP.Contains(AppConfig.SensorIPs[0].Substring(0, 11)))
                {
                    TextBoxLocalIP1.Text = HostIPs[ix].ToString();
                }
                else if (HostIP.Contains(AppConfig.SensorIPs[1].Substring(0, 11)))
                {
                    TextBoxLocalIP2.Text = HostIPs[ix].ToString();
                }
            }

            ButtonStartClient.Enabled = active;
            ButtonProgramRP.Enabled = active;
            ButtonStartServer.Enabled = active;

            PicSensor1.BackColor = ( (AppConfig.ActiveSensors & 0x01) != 0 ) ? (Color.Green) : (Color.Red);
            PicSensor2.BackColor = ( (AppConfig.ActiveSensors & 0x02) != 0 ) ? (Color.Green) : (Color.Red);

        }



        private void CbWindowWith_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (AppConfig.Active)
            {
                UDPSendData("-w " + CbWindowWith.SelectedItem.ToString(), 0);
                UDPSendData("-w " + CbWindowWith.SelectedItem.ToString(), 1);
            }
        }



        private void ButtonStartLogging_X_Click(object sender, EventArgs e)
        {
            string Cmd;
            Button Btn = (Button)sender;

            // saving of Data is toggled
            AppConfig.SaveStreamsActive ^= true;
            GbLiveMeasuring.Enabled = !AppConfig.SaveStreamsActive;

            switch ( Btn.Name)
            {
                case "ButtonStartLoggingRemote":
                    AppConfig.SaveStreamsLocal = false;
                    
                    break;
                case "ButtonStartLoggingLocal":
                    AppConfig.SaveStreamsLocal = false;
                    break;
                default:
                    break;
            }

            Cmd = "-l " + TbSecondsToMeasure.Text;
            UDPSendData(Cmd);
        }



        private void ButtonStartXLogging_Click(object sender, EventArgs e)
        {
            Button Btn = (Button)sender;
            String Cmd;

            switch(Btn.Name)
            {
                case "ButtonStartRemoteLogging":
                    Cmd = "-l " + TbSecondsToMeasure.Text + " " + (((CbFFT.Checked) ? (0x02) : (0x00)) | ((CbADC.Checked) ? (0x01) : (0x00))).ToString();
                    UDPSendData(Cmd);
                    break;

                case "0":

                    break;

                default:
                    break;
            }
        }



        private void ButtonAdcAutoWindowing_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox Cb = (CheckBox)sender;
            String Cmd = "-v " + ((Cb.Checked) ? ("1") : ("0"));
            Cb.BackColor = (Cb.Checked) ? (Color.LightGreen) : (Color.Transparent);
            UDPSendData(Cmd);
        }
    }


}
