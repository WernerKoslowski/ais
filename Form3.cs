﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinSCP;

namespace UDP_Client
{
    public partial class Form3 : Form
    {
        readonly String ProgDirRemote = "/root/iic";
        internal static Form1 FM1 = null;
        private string IpAddress;
        private int DownloadedFiles = 0;
        private float[] HeaderInfo2 = new float[3];

        public Form3()
        {
            InitializeComponent();
        }



        private void Form3_Load(object sender, EventArgs e)
        {
            this.AllowDrop = true;
            TbOutput.Text = "First download files, then open bin-files and just convert them";

            progressBar1.Maximum = 1000;
            progressBar1.Value   = 0;

            ToolTipConverting.SetToolTip(ButtonConvert, "Click to convert");
            ToolTipConverting.AutoPopDelay = 5000;
            ToolTipConverting.InitialDelay = 1000;
            ToolTipConverting.ReshowDelay = 500;
            ToolTipConverting.ShowAlways = true;
            IpAddress = "192.168.128.1";
            
        }

        
        private void Form3_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
        }



        private void Form3_DragDrop(object sender, DragEventArgs e)
        {
            string[] FileList;

            FileList = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            TB_Input.Text = FileList[0];
        }



        private void Button_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }



        private void Button_Convert_Click(object sender, EventArgs e)
        {
            string OutputFile = TB_Output.Text;
            int DataSetCounter = 0;
            int HeaderLength, ADCDataLength, FFTDataLength, FeatureLength;
            UInt64 ByteCount;
            Button Butt_In = (Button)(sender);
            Butt_In.Enabled = false;
            File.Delete(OutputFile);
            String Pattern = "\t";


            if ( File.Exists(TB_Input.Text))
            {

                try
                {

                    using (BinaryReader BiReader = new BinaryReader(File.OpenRead(TB_Input.Text)))
                    {

                        int Position = 0;
                        ByteCount = (UInt64)BiReader.BaseStream.Length;         // Length in byte of the whole Stream (File)
                        string Output;


                        while ((UInt64)Position < ByteCount)
                        {

                            HeaderLength = (int)BiReader.ReadSingle();                    // length of header in byte
                            ADCDataLength = (int)BiReader.ReadSingle();
                            FFTDataLength = (int)BiReader.ReadSingle();
                            FeatureLength = (int)BiReader.ReadSingle();


                            Output = (HeaderLength / 4).ToString() + Pattern + (ADCDataLength / 2).ToString() + Pattern + (FFTDataLength / 2).ToString() + Pattern + (FeatureLength/4).ToString() + Pattern;


                            // Read all header data
                            while (BiReader.BaseStream.Position < (Position + HeaderLength))
                            {
                                Output += BiReader.ReadSingle().ToString() + Pattern;
                            }
                            Position += HeaderLength;


                            // Read all ADC data
                            while (BiReader.BaseStream.Position < (Position + ADCDataLength))
                            {
                                Output += BiReader.ReadInt16().ToString() + Pattern;
                            }
                            Position += ADCDataLength;


                            // read all FFT data
                            while (BiReader.BaseStream.Position < (Position + FFTDataLength))
                            {
                                Output += BiReader.ReadUInt16().ToString() + Pattern;
                            }
                            Position += FFTDataLength;


                            // read all feature data
                            while (BiReader.BaseStream.Position < (Position + FeatureLength))
                            {
                                Output += BiReader.ReadSingle().ToString() + Pattern;
                            }
                            Position += FeatureLength;


                            Output += "\n";
                            File.AppendAllText(TB_Output.Text, Output);
                            DataSetCounter++;

                            progressBar1.Value = (int)(((double)Position / (double)ByteCount) * 1000.0);
                        }
                    }

                    ConvertingLabel.Text = TB_Output.Text;


                }
                catch
                {
                    // should, in best way, never be reached!
                }


                Butt_In.Enabled = true;

                TbOutput.Text = DataSetCounter.ToString() + " data sets are converted!";

            }
        }



        private void TB_Input_TextChanged(object sender, EventArgs e)
        {
            bool Checked = File.Exists(TB_Input.Text);

            ButtonConvert.Enabled = Checked;
            TB_Output.Text = (Checked) ? (TB_Input.Text.Substring(0, TB_Input.Text.Length - 4) + ".txt") : ("---");
        }



        private void ButtonOpenFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            String Tmp = Directory.GetCurrentDirectory();

            if(Directory.Exists(Tmp + "\\save"))
            {
                Tmp += "\\save";
            }

            openFileDialog.Filter = "Binfiles (*.bin)|*.bin";
            openFileDialog.InitialDirectory = Tmp;
            openFileDialog.ShowDialog();

            TbOutput.Text = "Now you can convert it!";
            
            TB_Input.Text = openFileDialog.FileName;

            return;
        }



        private void ButtonDownloadBinFiles_Click(object sender, EventArgs e)
        {
            WinSCP.Session SCP_Client = new WinSCP.Session();
            WinSCP.SessionOptions ScpSensorOptions;
            WinSCP.TransferOptions transferOptions = new TransferOptions();
            WinSCP.TransferOperationResult Result = null;

            TbOutput.Text = "downloading via SCP protocol. Please wait!";

            transferOptions.FileMask = "*.bin";
            ScpSensorOptions = new WinSCP.SessionOptions { Protocol = Protocol.Scp, HostName = IpAddress, UserName = "root", Password = "root", GiveUpSecurityAndAcceptAnySshHostKey = true, PortNumber = 22, };
            
            SCP_Client.Open(ScpSensorOptions);
            if (SCP_Client.FileExists(ProgDirRemote))
            {
                Result = SCP_Client.GetFilesToDirectory(ProgDirRemote, Directory.GetCurrentDirectory() + "\\save","*.bin",true);
            }
            else
            {
                TbOutput.Text = "Error - the folder does not exist!";
            }
            SCP_Client.Close();
            DownloadedFiles = Result.Transfers.Count;
            TbOutput.Text = (DownloadedFiles > 0)?(DownloadedFiles.ToString() + " new file(s)") :("No Data available!");
            ButtonConvert.Enabled = (DownloadedFiles > 0);
        }



        private void ConvertingLabel_Click(object sender, EventArgs e)
        {
            if( File.Exists( TB_Output.Text )  )
            {
                Process.Start(TB_Output.Text);
            }
            
        }
    }
}
