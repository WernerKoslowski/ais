﻿namespace UDP_Client
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.BoxConfig = new System.Windows.Forms.GroupBox();
            this.ButtonProgramRP = new System.Windows.Forms.Button();
            this.ButtonStartServer = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.ButtonCheckServer = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TextBoxRemotePort1 = new System.Windows.Forms.TextBox();
            this.TextBoxLocalIP2 = new System.Windows.Forms.TextBox();
            this.TextBoxLocalIP1 = new System.Windows.Forms.TextBox();
            this.TextBoxRemoteIP2 = new System.Windows.Forms.TextBox();
            this.TextBoxRemoteIP1 = new System.Windows.Forms.TextBox();
            this.BoxRxTx = new System.Windows.Forms.GroupBox();
            this.ButtonAdcAutoWindowing = new System.Windows.Forms.CheckBox();
            this.TbClass = new System.Windows.Forms.TextBox();
            this.TbDistance = new System.Windows.Forms.TextBox();
            this.GbLiveMeasuring = new System.Windows.Forms.GroupBox();
            this.ButtonStartRemoteLogging = new System.Windows.Forms.CheckBox();
            this.ButtonStartLocalLogging = new System.Windows.Forms.CheckBox();
            this.ButtonStartLiveMeasure = new System.Windows.Forms.CheckBox();
            this.TbSecondsToMeasure = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.CbFFT = new System.Windows.Forms.CheckBox();
            this.CbFeature = new System.Windows.Forms.CheckBox();
            this.CbADC = new System.Windows.Forms.CheckBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.CbLearnObject = new System.Windows.Forms.ComboBox();
            this.ButtonSendCmd = new System.Windows.Forms.Button();
            this.CbWindowWith = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.TbSendCmd = new System.Windows.Forms.TextBox();
            this.LabelRPSWVersion = new System.Windows.Forms.Label();
            this.TbFeatures = new System.Windows.Forms.TextBox();
            this.LabelRxPayload = new System.Windows.Forms.Label();
            this.ButtonStartConverter = new System.Windows.Forms.CheckBox();
            this.ButtonStartClient = new System.Windows.Forms.CheckBox();
            this.ButtonExit = new System.Windows.Forms.Button();
            this.BoxInformation = new System.Windows.Forms.GroupBox();
            this.LabelSaveDir = new System.Windows.Forms.Label();
            this.LabelProgDir = new System.Windows.Forms.Label();
            this.LabelWorkDir = new System.Windows.Forms.Label();
            this.LabelOpenDataDir = new System.Windows.Forms.Label();
            this.LabelOpenProgramDir = new System.Windows.Forms.Label();
            this.LabelOpenWorkingDir = new System.Windows.Forms.Label();
            this.LabelGUISWVersion = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.LableStatusGlobal = new System.Windows.Forms.Label();
            this.BoxChart = new System.Windows.Forms.GroupBox();
            this.ChartAdc = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.LabelPlotSpeed = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.LabelPlotCount = new System.Windows.Forms.Label();
            this.ChartFFT = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.TimerRxSpeed = new System.Windows.Forms.Timer(this.components);
            this.T1 = new System.Windows.Forms.Timer(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.PicSensor1 = new System.Windows.Forms.PictureBox();
            this.PicSensor2 = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.BoxConfig.SuspendLayout();
            this.BoxRxTx.SuspendLayout();
            this.GbLiveMeasuring.SuspendLayout();
            this.BoxInformation.SuspendLayout();
            this.BoxChart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChartAdc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChartFFT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicSensor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicSensor2)).BeginInit();
            this.SuspendLayout();
            // 
            // BoxConfig
            // 
            this.BoxConfig.Controls.Add(this.ButtonProgramRP);
            this.BoxConfig.Controls.Add(this.ButtonStartServer);
            this.BoxConfig.Controls.Add(this.label2);
            this.BoxConfig.Controls.Add(this.ButtonCheckServer);
            this.BoxConfig.Controls.Add(this.label7);
            this.BoxConfig.Controls.Add(this.label6);
            this.BoxConfig.Controls.Add(this.label9);
            this.BoxConfig.Controls.Add(this.label1);
            this.BoxConfig.Controls.Add(this.TextBoxRemotePort1);
            this.BoxConfig.Controls.Add(this.TextBoxLocalIP2);
            this.BoxConfig.Controls.Add(this.TextBoxLocalIP1);
            this.BoxConfig.Controls.Add(this.TextBoxRemoteIP2);
            this.BoxConfig.Controls.Add(this.TextBoxRemoteIP1);
            this.BoxConfig.Location = new System.Drawing.Point(16, 167);
            this.BoxConfig.Margin = new System.Windows.Forms.Padding(4);
            this.BoxConfig.Name = "BoxConfig";
            this.BoxConfig.Padding = new System.Windows.Forms.Padding(4);
            this.BoxConfig.Size = new System.Drawing.Size(316, 325);
            this.BoxConfig.TabIndex = 0;
            this.BoxConfig.TabStop = false;
            this.BoxConfig.Text = "UDP client config";
            // 
            // ButtonProgramRP
            // 
            this.ButtonProgramRP.Enabled = false;
            this.ButtonProgramRP.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonProgramRP.Location = new System.Drawing.Point(8, 249);
            this.ButtonProgramRP.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonProgramRP.Name = "ButtonProgramRP";
            this.ButtonProgramRP.Size = new System.Drawing.Size(280, 28);
            this.ButtonProgramRP.TabIndex = 0;
            this.ButtonProgramRP.Text = "program sensor";
            this.ButtonProgramRP.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonProgramRP.UseVisualStyleBackColor = true;
            this.ButtonProgramRP.Click += new System.EventHandler(this.ButtonProgramRP_Click);
            // 
            // ButtonStartServer
            // 
            this.ButtonStartServer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonStartServer.Location = new System.Drawing.Point(8, 284);
            this.ButtonStartServer.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonStartServer.Name = "ButtonStartServer";
            this.ButtonStartServer.Size = new System.Drawing.Size(280, 28);
            this.ButtonStartServer.TabIndex = 0;
            this.ButtonStartServer.Text = "(re-) start sensor";
            this.ButtonStartServer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonStartServer.UseVisualStyleBackColor = true;
            this.ButtonStartServer.Click += new System.EventHandler(this.ButtonStartSensor_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(8, 89);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Sensor port";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ButtonCheckServer
            // 
            this.ButtonCheckServer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonCheckServer.Location = new System.Drawing.Point(8, 213);
            this.ButtonCheckServer.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonCheckServer.Name = "ButtonCheckServer";
            this.ButtonCheckServer.Size = new System.Drawing.Size(280, 28);
            this.ButtonCheckServer.TabIndex = 0;
            this.ButtonCheckServer.Text = "check sensor connection";
            this.ButtonCheckServer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonCheckServer.UseVisualStyleBackColor = true;
            this.ButtonCheckServer.Click += new System.EventHandler(this.ButtonCheckSensor_Click);
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(8, 176);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(145, 25);
            this.label7.TabIndex = 1;
            this.label7.Text = "local IP-address 2";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(8, 143);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(145, 25);
            this.label6.TabIndex = 1;
            this.label6.Text = "local IP-address 1";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(8, 57);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(145, 25);
            this.label9.TabIndex = 1;
            this.label9.Text = "Sensor 2 IP-address";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 25);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Sensor 1 IP-address";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TextBoxRemotePort1
            // 
            this.TextBoxRemotePort1.Location = new System.Drawing.Point(175, 90);
            this.TextBoxRemotePort1.Margin = new System.Windows.Forms.Padding(4);
            this.TextBoxRemotePort1.Name = "TextBoxRemotePort1";
            this.TextBoxRemotePort1.Size = new System.Drawing.Size(112, 22);
            this.TextBoxRemotePort1.TabIndex = 1;
            this.TextBoxRemotePort1.Text = "0";
            this.TextBoxRemotePort1.TextChanged += new System.EventHandler(this.TextBoxRemotePort1_TextChanged);
            // 
            // TextBoxLocalIP2
            // 
            this.TextBoxLocalIP2.Location = new System.Drawing.Point(175, 177);
            this.TextBoxLocalIP2.Margin = new System.Windows.Forms.Padding(4);
            this.TextBoxLocalIP2.Name = "TextBoxLocalIP2";
            this.TextBoxLocalIP2.Size = new System.Drawing.Size(112, 22);
            this.TextBoxLocalIP2.TabIndex = 1;
            this.TextBoxLocalIP2.Text = "127.0.0.1";
            // 
            // TextBoxLocalIP1
            // 
            this.TextBoxLocalIP1.Location = new System.Drawing.Point(175, 144);
            this.TextBoxLocalIP1.Margin = new System.Windows.Forms.Padding(4);
            this.TextBoxLocalIP1.Name = "TextBoxLocalIP1";
            this.TextBoxLocalIP1.Size = new System.Drawing.Size(112, 22);
            this.TextBoxLocalIP1.TabIndex = 1;
            this.TextBoxLocalIP1.Text = "127.0.0.1";
            // 
            // TextBoxRemoteIP2
            // 
            this.TextBoxRemoteIP2.Location = new System.Drawing.Point(175, 58);
            this.TextBoxRemoteIP2.Margin = new System.Windows.Forms.Padding(4);
            this.TextBoxRemoteIP2.Name = "TextBoxRemoteIP2";
            this.TextBoxRemoteIP2.Size = new System.Drawing.Size(112, 22);
            this.TextBoxRemoteIP2.TabIndex = 1;
            this.TextBoxRemoteIP2.Text = "0.0.0.0";
            this.TextBoxRemoteIP2.TextChanged += new System.EventHandler(this.TextBoxRemoteIP2_TextChanged);
            // 
            // TextBoxRemoteIP1
            // 
            this.TextBoxRemoteIP1.Location = new System.Drawing.Point(175, 26);
            this.TextBoxRemoteIP1.Margin = new System.Windows.Forms.Padding(4);
            this.TextBoxRemoteIP1.Name = "TextBoxRemoteIP1";
            this.TextBoxRemoteIP1.Size = new System.Drawing.Size(112, 22);
            this.TextBoxRemoteIP1.TabIndex = 1;
            this.TextBoxRemoteIP1.Text = "0.0.0.0";
            this.TextBoxRemoteIP1.TextChanged += new System.EventHandler(this.TextBoxRemoteIP1_TextChanged);
            // 
            // BoxRxTx
            // 
            this.BoxRxTx.Controls.Add(this.ButtonAdcAutoWindowing);
            this.BoxRxTx.Controls.Add(this.TbClass);
            this.BoxRxTx.Controls.Add(this.TbDistance);
            this.BoxRxTx.Controls.Add(this.GbLiveMeasuring);
            this.BoxRxTx.Controls.Add(this.label17);
            this.BoxRxTx.Controls.Add(this.label8);
            this.BoxRxTx.Controls.Add(this.label16);
            this.BoxRxTx.Controls.Add(this.label3);
            this.BoxRxTx.Controls.Add(this.CbLearnObject);
            this.BoxRxTx.Controls.Add(this.ButtonSendCmd);
            this.BoxRxTx.Controls.Add(this.CbWindowWith);
            this.BoxRxTx.Controls.Add(this.label10);
            this.BoxRxTx.Controls.Add(this.TbSendCmd);
            this.BoxRxTx.Controls.Add(this.LabelRPSWVersion);
            this.BoxRxTx.Enabled = false;
            this.BoxRxTx.Location = new System.Drawing.Point(340, 167);
            this.BoxRxTx.Margin = new System.Windows.Forms.Padding(4);
            this.BoxRxTx.Name = "BoxRxTx";
            this.BoxRxTx.Padding = new System.Windows.Forms.Padding(4);
            this.BoxRxTx.Size = new System.Drawing.Size(320, 719);
            this.BoxRxTx.TabIndex = 1;
            this.BoxRxTx.TabStop = false;
            this.BoxRxTx.Text = "Send / Receive";
            // 
            // ButtonAdcAutoWindowing
            // 
            this.ButtonAdcAutoWindowing.Appearance = System.Windows.Forms.Appearance.Button;
            this.ButtonAdcAutoWindowing.Location = new System.Drawing.Point(15, 94);
            this.ButtonAdcAutoWindowing.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonAdcAutoWindowing.Name = "ButtonAdcAutoWindowing";
            this.ButtonAdcAutoWindowing.Size = new System.Drawing.Size(161, 28);
            this.ButtonAdcAutoWindowing.TabIndex = 11;
            this.ButtonAdcAutoWindowing.Text = "Auto object windowing";
            this.ButtonAdcAutoWindowing.UseVisualStyleBackColor = true;
            this.ButtonAdcAutoWindowing.CheckedChanged += new System.EventHandler(this.ButtonAdcAutoWindowing_CheckedChanged);
            // 
            // TbClass
            // 
            this.TbClass.Location = new System.Drawing.Point(224, 465);
            this.TbClass.Name = "TbClass";
            this.TbClass.Size = new System.Drawing.Size(82, 22);
            this.TbClass.TabIndex = 18;
            // 
            // TbDistance
            // 
            this.TbDistance.Location = new System.Drawing.Point(224, 437);
            this.TbDistance.Name = "TbDistance";
            this.TbDistance.Size = new System.Drawing.Size(82, 22);
            this.TbDistance.TabIndex = 18;
            // 
            // GbLiveMeasuring
            // 
            this.GbLiveMeasuring.Controls.Add(this.ButtonStartRemoteLogging);
            this.GbLiveMeasuring.Controls.Add(this.ButtonStartLocalLogging);
            this.GbLiveMeasuring.Controls.Add(this.ButtonStartLiveMeasure);
            this.GbLiveMeasuring.Controls.Add(this.TbSecondsToMeasure);
            this.GbLiveMeasuring.Controls.Add(this.label15);
            this.GbLiveMeasuring.Controls.Add(this.CbFFT);
            this.GbLiveMeasuring.Controls.Add(this.CbFeature);
            this.GbLiveMeasuring.Controls.Add(this.CbADC);
            this.GbLiveMeasuring.Location = new System.Drawing.Point(8, 213);
            this.GbLiveMeasuring.Name = "GbLiveMeasuring";
            this.GbLiveMeasuring.Size = new System.Drawing.Size(305, 199);
            this.GbLiveMeasuring.TabIndex = 17;
            this.GbLiveMeasuring.TabStop = false;
            this.GbLiveMeasuring.Text = "Measuring";
            // 
            // ButtonStartRemoteLogging
            // 
            this.ButtonStartRemoteLogging.Appearance = System.Windows.Forms.Appearance.Button;
            this.ButtonStartRemoteLogging.Location = new System.Drawing.Point(7, 94);
            this.ButtonStartRemoteLogging.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonStartRemoteLogging.Name = "ButtonStartRemoteLogging";
            this.ButtonStartRemoteLogging.Size = new System.Drawing.Size(145, 28);
            this.ButtonStartRemoteLogging.TabIndex = 11;
            this.ButtonStartRemoteLogging.Text = "logging remote";
            this.ButtonStartRemoteLogging.UseVisualStyleBackColor = true;
            this.ButtonStartRemoteLogging.CheckedChanged += new System.EventHandler(this.ButtonStartX_Click);
            // 
            // ButtonStartLocalLogging
            // 
            this.ButtonStartLocalLogging.Appearance = System.Windows.Forms.Appearance.Button;
            this.ButtonStartLocalLogging.Location = new System.Drawing.Point(7, 58);
            this.ButtonStartLocalLogging.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonStartLocalLogging.Name = "ButtonStartLocalLogging";
            this.ButtonStartLocalLogging.Size = new System.Drawing.Size(145, 28);
            this.ButtonStartLocalLogging.TabIndex = 11;
            this.ButtonStartLocalLogging.Text = "logging local";
            this.ButtonStartLocalLogging.UseVisualStyleBackColor = true;
            this.ButtonStartLocalLogging.CheckedChanged += new System.EventHandler(this.ButtonStartX_Click);
            // 
            // ButtonStartLiveMeasure
            // 
            this.ButtonStartLiveMeasure.Appearance = System.Windows.Forms.Appearance.Button;
            this.ButtonStartLiveMeasure.Location = new System.Drawing.Point(7, 22);
            this.ButtonStartLiveMeasure.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonStartLiveMeasure.Name = "ButtonStartLiveMeasure";
            this.ButtonStartLiveMeasure.Size = new System.Drawing.Size(145, 28);
            this.ButtonStartLiveMeasure.TabIndex = 11;
            this.ButtonStartLiveMeasure.Text = "live measurement";
            this.ButtonStartLiveMeasure.UseVisualStyleBackColor = true;
            this.ButtonStartLiveMeasure.CheckedChanged += new System.EventHandler(this.ButtonStartX_Click);
            // 
            // TbSecondsToMeasure
            // 
            this.TbSecondsToMeasure.Location = new System.Drawing.Point(239, 127);
            this.TbSecondsToMeasure.Margin = new System.Windows.Forms.Padding(4);
            this.TbSecondsToMeasure.Name = "TbSecondsToMeasure";
            this.TbSecondsToMeasure.Size = new System.Drawing.Size(59, 22);
            this.TbSecondsToMeasure.TabIndex = 1;
            this.TbSecondsToMeasure.Text = "-1";
            this.TbSecondsToMeasure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 130);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(136, 17);
            this.label15.TabIndex = 0;
            this.label15.Text = "seconds to measure";
            this.label15.Click += new System.EventHandler(this.LabelSaveDir_Click);
            // 
            // CbFFT
            // 
            this.CbFFT.AutoSize = true;
            this.CbFFT.Location = new System.Drawing.Point(198, 54);
            this.CbFFT.Name = "CbFFT";
            this.CbFFT.Size = new System.Drawing.Size(87, 21);
            this.CbFFT.TabIndex = 15;
            this.CbFFT.Text = "FFT data";
            this.CbFFT.UseVisualStyleBackColor = true;
            // 
            // CbFeature
            // 
            this.CbFeature.AutoSize = true;
            this.CbFeature.Location = new System.Drawing.Point(198, 81);
            this.CbFeature.Name = "CbFeature";
            this.CbFeature.Size = new System.Drawing.Size(107, 21);
            this.CbFeature.TabIndex = 15;
            this.CbFeature.Text = "feature data";
            this.CbFeature.UseVisualStyleBackColor = true;
            // 
            // CbADC
            // 
            this.CbADC.AutoSize = true;
            this.CbADC.Location = new System.Drawing.Point(198, 27);
            this.CbADC.Name = "CbADC";
            this.CbADC.Size = new System.Drawing.Size(90, 21);
            this.CbADC.TabIndex = 15;
            this.CbADC.Text = "ADC data";
            this.CbADC.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(15, 153);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(118, 17);
            this.label17.TabIndex = 0;
            this.label17.Text = "FFT window width";
            this.label17.Click += new System.EventHandler(this.LabelSaveDir_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 185);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(98, 17);
            this.label8.TabIndex = 0;
            this.label8.Text = "object to learn";
            this.label8.Click += new System.EventHandler(this.LabelSaveDir_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(17, 469);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(40, 17);
            this.label16.TabIndex = 16;
            this.label16.Text = "class";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 440);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 17);
            this.label3.TabIndex = 16;
            this.label3.Text = "distance";
            // 
            // CbLearnObject
            // 
            this.CbLearnObject.FormattingEnabled = true;
            this.CbLearnObject.Location = new System.Drawing.Point(211, 182);
            this.CbLearnObject.Margin = new System.Windows.Forms.Padding(4);
            this.CbLearnObject.Name = "CbLearnObject";
            this.CbLearnObject.Size = new System.Drawing.Size(95, 24);
            this.CbLearnObject.TabIndex = 12;
            // 
            // ButtonSendCmd
            // 
            this.ButtonSendCmd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonSendCmd.Location = new System.Drawing.Point(131, 55);
            this.ButtonSendCmd.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonSendCmd.Name = "ButtonSendCmd";
            this.ButtonSendCmd.Size = new System.Drawing.Size(91, 28);
            this.ButtonSendCmd.TabIndex = 0;
            this.ButtonSendCmd.Text = "send cmd";
            this.ButtonSendCmd.UseVisualStyleBackColor = true;
            this.ButtonSendCmd.Click += new System.EventHandler(this.ButtonSendCmd_Click);
            // 
            // CbWindowWith
            // 
            this.CbWindowWith.FormattingEnabled = true;
            this.CbWindowWith.Location = new System.Drawing.Point(211, 150);
            this.CbWindowWith.Margin = new System.Windows.Forms.Padding(4);
            this.CbWindowWith.Name = "CbWindowWith";
            this.CbWindowWith.Size = new System.Drawing.Size(95, 24);
            this.CbWindowWith.TabIndex = 12;
            this.CbWindowWith.SelectedIndexChanged += new System.EventHandler(this.CbWindowWith_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 30);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(135, 17);
            this.label10.TabIndex = 0;
            this.label10.Text = "Sensor SW Version:";
            this.label10.Click += new System.EventHandler(this.LabelSaveDir_Click);
            // 
            // TbSendCmd
            // 
            this.TbSendCmd.Location = new System.Drawing.Point(20, 58);
            this.TbSendCmd.Margin = new System.Windows.Forms.Padding(4);
            this.TbSendCmd.Name = "TbSendCmd";
            this.TbSendCmd.Size = new System.Drawing.Size(101, 22);
            this.TbSendCmd.TabIndex = 1;
            this.TbSendCmd.Text = "-l 10";
            // 
            // LabelRPSWVersion
            // 
            this.LabelRPSWVersion.AutoSize = true;
            this.LabelRPSWVersion.Location = new System.Drawing.Point(152, 30);
            this.LabelRPSWVersion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelRPSWVersion.Name = "LabelRPSWVersion";
            this.LabelRPSWVersion.Size = new System.Drawing.Size(16, 17);
            this.LabelRPSWVersion.TabIndex = 0;
            this.LabelRPSWVersion.Text = "0";
            this.LabelRPSWVersion.Click += new System.EventHandler(this.LabelVersionInfo_Click);
            // 
            // TbFeatures
            // 
            this.TbFeatures.Location = new System.Drawing.Point(7, 538);
            this.TbFeatures.Multiline = true;
            this.TbFeatures.Name = "TbFeatures";
            this.TbFeatures.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.TbFeatures.Size = new System.Drawing.Size(666, 85);
            this.TbFeatures.TabIndex = 19;
            // 
            // LabelRxPayload
            // 
            this.LabelRxPayload.Location = new System.Drawing.Point(8, 626);
            this.LabelRxPayload.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelRxPayload.Name = "LabelRxPayload";
            this.LabelRxPayload.Size = new System.Drawing.Size(665, 25);
            this.LabelRxPayload.TabIndex = 1;
            this.LabelRxPayload.Text = "0";
            this.LabelRxPayload.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ButtonStartConverter
            // 
            this.ButtonStartConverter.Appearance = System.Windows.Forms.Appearance.Button;
            this.ButtonStartConverter.Location = new System.Drawing.Point(16, 550);
            this.ButtonStartConverter.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonStartConverter.Name = "ButtonStartConverter";
            this.ButtonStartConverter.Size = new System.Drawing.Size(123, 28);
            this.ButtonStartConverter.TabIndex = 11;
            this.ButtonStartConverter.Text = "start converter";
            this.ButtonStartConverter.UseVisualStyleBackColor = true;
            this.ButtonStartConverter.CheckedChanged += new System.EventHandler(this.ButtonStartX_Click);
            // 
            // ButtonStartClient
            // 
            this.ButtonStartClient.Appearance = System.Windows.Forms.Appearance.Button;
            this.ButtonStartClient.AutoSize = true;
            this.ButtonStartClient.Location = new System.Drawing.Point(106, 859);
            this.ButtonStartClient.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonStartClient.Name = "ButtonStartClient";
            this.ButtonStartClient.Size = new System.Drawing.Size(83, 27);
            this.ButtonStartClient.TabIndex = 2;
            this.ButtonStartClient.Text = "start client";
            this.ButtonStartClient.UseVisualStyleBackColor = true;
            this.ButtonStartClient.CheckedChanged += new System.EventHandler(this.ButtonStartUDPClient_CheckedChanged);
            // 
            // ButtonExit
            // 
            this.ButtonExit.Location = new System.Drawing.Point(13, 858);
            this.ButtonExit.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonExit.Name = "ButtonExit";
            this.ButtonExit.Size = new System.Drawing.Size(85, 28);
            this.ButtonExit.TabIndex = 3;
            this.ButtonExit.Text = "exit app";
            this.ButtonExit.UseVisualStyleBackColor = true;
            this.ButtonExit.Click += new System.EventHandler(this.ButtonExit_Click);
            // 
            // BoxInformation
            // 
            this.BoxInformation.Controls.Add(this.LabelSaveDir);
            this.BoxInformation.Controls.Add(this.LabelProgDir);
            this.BoxInformation.Controls.Add(this.LabelWorkDir);
            this.BoxInformation.Controls.Add(this.LabelOpenDataDir);
            this.BoxInformation.Controls.Add(this.LabelOpenProgramDir);
            this.BoxInformation.Controls.Add(this.LabelOpenWorkingDir);
            this.BoxInformation.Controls.Add(this.LabelGUISWVersion);
            this.BoxInformation.Controls.Add(this.label12);
            this.BoxInformation.Controls.Add(this.label5);
            this.BoxInformation.Location = new System.Drawing.Point(340, 15);
            this.BoxInformation.Margin = new System.Windows.Forms.Padding(4);
            this.BoxInformation.Name = "BoxInformation";
            this.BoxInformation.Padding = new System.Windows.Forms.Padding(4);
            this.BoxInformation.Size = new System.Drawing.Size(1009, 145);
            this.BoxInformation.TabIndex = 5;
            this.BoxInformation.TabStop = false;
            this.BoxInformation.Text = "Information";
            // 
            // LabelSaveDir
            // 
            this.LabelSaveDir.AutoSize = true;
            this.LabelSaveDir.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelSaveDir.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelSaveDir.Location = new System.Drawing.Point(131, 123);
            this.LabelSaveDir.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelSaveDir.Name = "LabelSaveDir";
            this.LabelSaveDir.Size = new System.Drawing.Size(66, 19);
            this.LabelSaveDir.TabIndex = 1;
            this.LabelSaveDir.Text = "DataDir";
            this.LabelSaveDir.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LabelProgDir
            // 
            this.LabelProgDir.AutoSize = true;
            this.LabelProgDir.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelProgDir.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelProgDir.Location = new System.Drawing.Point(129, 98);
            this.LabelProgDir.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelProgDir.Name = "LabelProgDir";
            this.LabelProgDir.Size = new System.Drawing.Size(66, 19);
            this.LabelProgDir.TabIndex = 1;
            this.LabelProgDir.Text = "ProgDir";
            this.LabelProgDir.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LabelWorkDir
            // 
            this.LabelWorkDir.AutoSize = true;
            this.LabelWorkDir.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelWorkDir.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelWorkDir.Location = new System.Drawing.Point(129, 74);
            this.LabelWorkDir.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelWorkDir.Name = "LabelWorkDir";
            this.LabelWorkDir.Size = new System.Drawing.Size(90, 19);
            this.LabelWorkDir.TabIndex = 1;
            this.LabelWorkDir.Text = "WorkingDir";
            this.LabelWorkDir.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.LabelWorkDir.Click += new System.EventHandler(this.LabelWorkingDir_Click);
            // 
            // LabelOpenDataDir
            // 
            this.LabelOpenDataDir.Location = new System.Drawing.Point(9, 117);
            this.LabelOpenDataDir.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelOpenDataDir.Name = "LabelOpenDataDir";
            this.LabelOpenDataDir.Size = new System.Drawing.Size(113, 25);
            this.LabelOpenDataDir.TabIndex = 1;
            this.LabelOpenDataDir.Text = "Data Dir:";
            this.LabelOpenDataDir.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.LabelOpenDataDir.Click += new System.EventHandler(this.LabelOpenDataDir_Click);
            // 
            // LabelOpenProgramDir
            // 
            this.LabelOpenProgramDir.Location = new System.Drawing.Point(9, 92);
            this.LabelOpenProgramDir.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelOpenProgramDir.Name = "LabelOpenProgramDir";
            this.LabelOpenProgramDir.Size = new System.Drawing.Size(113, 25);
            this.LabelOpenProgramDir.TabIndex = 1;
            this.LabelOpenProgramDir.Text = "Prog Dir:";
            this.LabelOpenProgramDir.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.LabelOpenProgramDir.Click += new System.EventHandler(this.LabelOpenProgramDir_Click);
            // 
            // LabelOpenWorkingDir
            // 
            this.LabelOpenWorkingDir.Location = new System.Drawing.Point(9, 68);
            this.LabelOpenWorkingDir.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelOpenWorkingDir.Name = "LabelOpenWorkingDir";
            this.LabelOpenWorkingDir.Size = new System.Drawing.Size(112, 25);
            this.LabelOpenWorkingDir.TabIndex = 1;
            this.LabelOpenWorkingDir.Text = "Working dir:";
            this.LabelOpenWorkingDir.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.LabelOpenWorkingDir.Click += new System.EventHandler(this.LabelOpenWorkingDir_Click);
            // 
            // LabelGUISWVersion
            // 
            this.LabelGUISWVersion.AutoSize = true;
            this.LabelGUISWVersion.Location = new System.Drawing.Point(131, 47);
            this.LabelGUISWVersion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelGUISWVersion.Name = "LabelGUISWVersion";
            this.LabelGUISWVersion.Size = new System.Drawing.Size(45, 17);
            this.LabelGUISWVersion.TabIndex = 0;
            this.LabelGUISWVersion.Text = "V0.22";
            this.LabelGUISWVersion.Click += new System.EventHandler(this.LabelVersionInfo_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 47);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(110, 17);
            this.label12.TabIndex = 0;
            this.label12.Text = "GUI SW Version";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 20);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(135, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "UAS Frankfurt - FB2";
            // 
            // LableStatusGlobal
            // 
            this.LableStatusGlobal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LableStatusGlobal.Location = new System.Drawing.Point(16, 496);
            this.LableStatusGlobal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LableStatusGlobal.Name = "LableStatusGlobal";
            this.LableStatusGlobal.Size = new System.Drawing.Size(316, 52);
            this.LableStatusGlobal.TabIndex = 1;
            this.LableStatusGlobal.Text = "info:";
            // 
            // BoxChart
            // 
            this.BoxChart.Controls.Add(this.TbFeatures);
            this.BoxChart.Controls.Add(this.ChartAdc);
            this.BoxChart.Controls.Add(this.LabelPlotSpeed);
            this.BoxChart.Controls.Add(this.label13);
            this.BoxChart.Controls.Add(this.label11);
            this.BoxChart.Controls.Add(this.LabelPlotCount);
            this.BoxChart.Controls.Add(this.ChartFFT);
            this.BoxChart.Controls.Add(this.LabelRxPayload);
            this.BoxChart.Enabled = false;
            this.BoxChart.Location = new System.Drawing.Point(668, 167);
            this.BoxChart.Margin = new System.Windows.Forms.Padding(4);
            this.BoxChart.Name = "BoxChart";
            this.BoxChart.Padding = new System.Windows.Forms.Padding(4);
            this.BoxChart.Size = new System.Drawing.Size(681, 719);
            this.BoxChart.TabIndex = 6;
            this.BoxChart.TabStop = false;
            this.BoxChart.Text = "Data ADC / FFT";
            // 
            // ChartAdc
            // 
            this.ChartAdc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea3.AxisY.Maximum = 1000D;
            chartArea3.AxisY.Minimum = 0D;
            chartArea3.Name = "ChartArea0";
            this.ChartAdc.ChartAreas.Add(chartArea3);
            this.ChartAdc.Location = new System.Drawing.Point(8, 23);
            this.ChartAdc.Margin = new System.Windows.Forms.Padding(4);
            this.ChartAdc.Name = "ChartAdc";
            series3.ChartArea = "ChartArea0";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.IsVisibleInLegend = false;
            series3.Name = "Series0";
            series3.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series3.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            this.ChartAdc.Series.Add(series3);
            this.ChartAdc.Size = new System.Drawing.Size(665, 250);
            this.ChartAdc.TabIndex = 2;
            this.ChartAdc.Text = "ChartDiagramm";
            // 
            // LabelPlotSpeed
            // 
            this.LabelPlotSpeed.Location = new System.Drawing.Point(171, 690);
            this.LabelPlotSpeed.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelPlotSpeed.Name = "LabelPlotSpeed";
            this.LabelPlotSpeed.Size = new System.Drawing.Size(73, 25);
            this.LabelPlotSpeed.TabIndex = 1;
            this.LabelPlotSpeed.Text = "0";
            this.LabelPlotSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(8, 690);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(155, 25);
            this.label13.TabIndex = 1;
            this.label13.Text = "Plots per second:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(8, 665);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 25);
            this.label11.TabIndex = 1;
            this.label11.Text = "Plots:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LabelPlotCount
            // 
            this.LabelPlotCount.Location = new System.Drawing.Point(171, 665);
            this.LabelPlotCount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelPlotCount.Name = "LabelPlotCount";
            this.LabelPlotCount.Size = new System.Drawing.Size(73, 25);
            this.LabelPlotCount.TabIndex = 1;
            this.LabelPlotCount.Text = "0";
            this.LabelPlotCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChartFFT
            // 
            this.ChartFFT.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea4.AxisY.Maximum = 1000D;
            chartArea4.AxisY.Minimum = 0D;
            chartArea4.Name = "ChartArea0";
            this.ChartFFT.ChartAreas.Add(chartArea4);
            this.ChartFFT.Location = new System.Drawing.Point(8, 281);
            this.ChartFFT.Margin = new System.Windows.Forms.Padding(4);
            this.ChartFFT.Name = "ChartFFT";
            series4.ChartArea = "ChartArea0";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series4.IsVisibleInLegend = false;
            series4.Name = "Series0";
            series4.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series4.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            this.ChartFFT.Series.Add(series4);
            this.ChartFFT.Size = new System.Drawing.Size(665, 250);
            this.ChartFFT.TabIndex = 0;
            this.ChartFFT.Text = "ChartDiagramm";
            // 
            // TimerRxSpeed
            // 
            this.TimerRxSpeed.Tick += new System.EventHandler(this.TimerRxSpeed_Tick);
            // 
            // T1
            // 
            this.T1.Tick += new System.EventHandler(this.T1_Tick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::UDP_Client.Properties.Resources.UAS_Logo;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(16, 15);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(13, 12, 13, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(316, 145);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // PicSensor1
            // 
            this.PicSensor1.BackColor = System.Drawing.Color.Red;
            this.PicSensor1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PicSensor1.Location = new System.Drawing.Point(103, 803);
            this.PicSensor1.Margin = new System.Windows.Forms.Padding(4);
            this.PicSensor1.Name = "PicSensor1";
            this.PicSensor1.Size = new System.Drawing.Size(20, 20);
            this.PicSensor1.TabIndex = 13;
            this.PicSensor1.TabStop = false;
            // 
            // PicSensor2
            // 
            this.PicSensor2.BackColor = System.Drawing.Color.Red;
            this.PicSensor2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PicSensor2.Location = new System.Drawing.Point(103, 831);
            this.PicSensor2.Margin = new System.Windows.Forms.Padding(4);
            this.PicSensor2.Name = "PicSensor2";
            this.PicSensor2.Size = new System.Drawing.Size(20, 20);
            this.PicSensor2.TabIndex = 13;
            this.PicSensor2.TabStop = false;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(16, 797);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 25);
            this.label4.TabIndex = 1;
            this.label4.Text = "Sensor 1:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(16, 829);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(79, 25);
            this.label14.TabIndex = 1;
            this.label14.Text = "Sensor 2:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Form1
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1365, 899);
            this.Controls.Add(this.PicSensor2);
            this.Controls.Add(this.PicSensor1);
            this.Controls.Add(this.BoxChart);
            this.Controls.Add(this.LableStatusGlobal);
            this.Controls.Add(this.BoxInformation);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.ButtonStartConverter);
            this.Controls.Add(this.ButtonExit);
            this.Controls.Add(this.ButtonStartClient);
            this.Controls.Add(this.BoxRxTx);
            this.Controls.Add(this.BoxConfig);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "UDP client - UAS Frankfurt @2020";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_Close);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.BoxConfig.ResumeLayout(false);
            this.BoxConfig.PerformLayout();
            this.BoxRxTx.ResumeLayout(false);
            this.BoxRxTx.PerformLayout();
            this.GbLiveMeasuring.ResumeLayout(false);
            this.GbLiveMeasuring.PerformLayout();
            this.BoxInformation.ResumeLayout(false);
            this.BoxInformation.PerformLayout();
            this.BoxChart.ResumeLayout(false);
            this.BoxChart.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChartAdc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChartFFT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicSensor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicSensor2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox BoxConfig;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TextBoxRemoteIP1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TextBoxRemotePort1;
        private System.Windows.Forms.GroupBox BoxRxTx;
        private System.Windows.Forms.CheckBox ButtonStartClient;
        private System.Windows.Forms.Button ButtonExit;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox BoxInformation;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TextBoxLocalIP1;
        private System.Windows.Forms.Button ButtonStartServer;
        private System.Windows.Forms.Button ButtonCheckServer;
        private System.Windows.Forms.Label LabelRPSWVersion;
        private System.Windows.Forms.Button ButtonProgramRP;
        private System.Windows.Forms.Label LabelWorkDir;
        private System.Windows.Forms.Label LabelOpenWorkingDir;
        private System.Windows.Forms.Label LabelProgDir;
        private System.Windows.Forms.Label LabelOpenProgramDir;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label LableStatusGlobal;
        private System.Windows.Forms.GroupBox BoxChart;
        private System.Windows.Forms.Label LabelSaveDir;
        private System.Windows.Forms.Label LabelOpenDataDir;
        private System.Windows.Forms.Timer TimerRxSpeed;
        private System.Windows.Forms.Label LabelPlotCount;
        private System.Windows.Forms.Timer T1;
        private System.Windows.Forms.Label LabelPlotSpeed;
        private System.Windows.Forms.CheckBox ButtonStartLiveMeasure;
        private System.Windows.Forms.CheckBox ButtonStartConverter;
        private System.Windows.Forms.Label LabelRxPayload;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TextBoxRemoteIP2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TextBoxLocalIP2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label LabelGUISWVersion;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button ButtonSendCmd;
        private System.Windows.Forms.TextBox TbSendCmd;
        private System.Windows.Forms.PictureBox PicSensor1;
        private System.Windows.Forms.PictureBox PicSensor2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox CbWindowWith;
        private System.Windows.Forms.CheckBox CbADC;
        private System.Windows.Forms.CheckBox CbFFT;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox TbSecondsToMeasure;
        private System.Windows.Forms.ComboBox CbLearnObject;
        private System.Windows.Forms.GroupBox GbLiveMeasuring;
        private System.Windows.Forms.TextBox TbDistance;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TbClass;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox ButtonAdcAutoWindowing;
        private System.Windows.Forms.TextBox TbFeatures;
        private System.Windows.Forms.CheckBox CbFeature;
        private System.Windows.Forms.CheckBox ButtonStartRemoteLogging;
        private System.Windows.Forms.CheckBox ButtonStartLocalLogging;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.DataVisualization.Charting.Chart ChartAdc;
        public System.Windows.Forms.DataVisualization.Charting.Chart ChartFFT;
    }
}

