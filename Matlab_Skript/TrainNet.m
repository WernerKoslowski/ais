
clearvars -except Const*;
clc;

ConstCountDataAll    = size( ConstFftMatrix,1 );

% initialize all necessary memory space
TrainingAllDataX            = ConstFftMatrix;
TrainingAllDataY            = ConstFftVector;
TrainingAllDataYLabels      = categorical( TrainingAllDataY );


%% Training part of data
% get all necessary matrices, vactors and labels




TrainValidRation = 0.1; % per cent

% get x percent as validation Data.
TmpValidationIndizes = uint32( randperm( size(TrainingAllDataX,1), uint32( TrainValidRation * length(TrainingAllDataX) ) ) );

ValidationDataX = TrainingAllDataX(TmpValidationIndizes,:);
ValidationDataY = TrainingAllDataY(TmpValidationIndizes,:);
ValidDataYLabels = categorical( ValidationDataY );

TrainingDataX          = TrainingAllDataX;
TrainingDataY          = TrainingAllDataY;
TrainingDataYLabels    = TrainingAllDataYLabels;

TrainingDataX(TmpValidationIndizes,:)       = [];
TrainingDataY(TmpValidationIndizes,:)       = [];
TrainingDataYLabels(TmpValidationIndizes,:) = [];

ConstCountDataTrain  = size( TrainingDataX,   1 );
ConstCountDataValid  = size( ValidationDataX,   1 );


%% now train the network
% init layers for learning
LearnLayers = [
    featureInputLayer(64,"Name","featureinput")
    fullyConnectedLayer(3,"Name","fc")
    softmaxLayer("Name","softmax")
    classificationLayer("Name","classoutput")];
% init options for learning
LearnOptions = trainingOptions('sgdm', ...
    MaxEpochs           = 10, ...
    InitialLearnRate    = 0.4,...
    ValidationFrequency = 75, ...
    ValidationData      = { ValidationDataX, ValidDataYLabels },...
    Verbose             = true, ...
    VerboseFrequency    = 100, ...
    Plots               = 'training-progress');

    
ConstTrainNetz = trainNetwork( TrainingDataX, TrainingDataYLabels, LearnLayers, LearnOptions );


% now test prediction with given known samples / classes
PredictDataY = predict(ConstTrainNetz,TrainingAllDataX);
TmpA = zeros( size(PredictDataY,1),1 );

for TmpIx1=1:1:size(TmpA)
    [TmpR, TmpC] = max( PredictDataY(TmpIx1,:) );
    PredictDataY(TmpIx1,1) = TmpC - 1;
end

% now we have predicted classes for all 
ConstClassesPredicted = double(PredictDataY(:,1));       % predicted classes from NN
ConstClassesKnown     = TrainingAllDataY;                   % known classes from data set

ConstConfusionMatrix = confusionmat( ConstClassesKnown, ConstClassesPredicted );
confusionchart( ConstConfusionMatrix );

clearvars -except Const*;

