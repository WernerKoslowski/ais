% Task 1
% Aufgabe 1a)
clearvars;
clc;

CurFolder  = string( pwd );

FileFolder ="C:\Users\Werner\Desktop\TmpMessungen";
%FileFolder = CurFolder;

FileInfos = dir(FileFolder + "\*.bin");
FileCount = length(FileInfos);

% Sonic speed in air (20°C & 1013 mBar) [m/s]
Param_V_SonicSpeed = 343.5;



% general information about the data
ParamFSampling = double( 125000000 / 64 );                                      % SamplingFrequency [1/s]
ParamHammWindowWidth        = uint16( 128 );
ParamMinimumDistance        = double( 0.3 );                               % minimum distance is 0.30 meter

ParamBufferSize            = 2^14;
ParamMinSensorDistance     = 0.30;                                             % minimum distance [m]

% header of every data set is 72 byte long
Param_HeaderLengthByte = 72;
Param_AdcLengthByte    = 32768;

% variable initialization
AdcData                 = int16( zeros(2^14,1) );
AdcDataWindow           = double( zeros(ParamHammWindowWidth,1));
MeasFftPerDataSetCount  = ( ParamBufferSize ) / ( ParamHammWindowWidth/2 ) - 1;

MeasFftDataCountAllFiles = 0;
% get the amount of measurements over ALL files
for FileIndex=1:FileCount
    MeasFftDataCountAllFiles = MeasFftDataCountAllFiles + ( FileInfos( FileIndex ).bytes / (Param_HeaderLengthByte + Param_AdcLengthByte) );
end


ConstFftMatrix               = double( zeros(MeasFftDataCountAllFiles * 6, ParamHammWindowWidth/2) ); % [4800, 511, 64]
ConstFftVector               = double( zeros(MeasFftDataCountAllFiles * 6,1) );


MeasFileCounter             = uint32(0);
ConstValidMeasurements      = double(0);
ConstInvalidMeasurements    = double(0);

%% loop through every file in given folder
for FileIndex=1:FileCount
    FileNameFolder      = FileInfos(FileIndex).folder;
    FileNameChar        = FileInfos(FileIndex).name;
    FileNameStr         = string(FileNameChar);
    FileDistance        = double( extractBefore( FileNameStr, strlength(FileNameStr)-3) ); % get the distance from the name of the fil
    ObjIndex            = uint16( ((FileDistance - ParamMinimumDistance) * ParamFSampling * 2.0) / Param_V_SonicSpeed ) ;
    ObjIndexMax         = ObjIndex + uint16( 3 * ParamHammWindowWidth / 4 );
    ObjIndexMin         = ObjIndex - uint16( 7 * ParamHammWindowWidth / 4 );

    ConstObjIndex(FileIndex) = ObjIndex;
    
    Fr = matlab.io.datastore.DsFileReader( FileNameFolder + "\" + FileNameChar );
    
    %% loop through every data set inside the current file
    while( Fr.Position < Fr.Size )

        %read out the header data of the current data set
        Tmp_Header              = read(Fr,Param_HeaderLengthByte,'OutputType','single');
        Tmp_AdcLengthByte       = uint16(Tmp_Header( 2));
        Tmp_FftLengthByte       = uint16(Tmp_Header( 3));
        Tmp_FeatureLengthByte   = uint16(Tmp_Header( 4));
        Tmp_Distance            = single( round( single(Tmp_Header(13)), 4) );       % has to be read out for every measurement
        MeasFFTCounter          = uint32(0);

        if( (Tmp_AdcLengthByte > 0) && ( abs( FileDistance - Tmp_Distance ) < 0.1 ) )
            % Current measurement is valid and has data
            ConstValidMeasurements = ConstValidMeasurements + 1;

            AdcData =  read( Fr, Tmp_AdcLengthByte, "OutputType","int16");
            AdcDataLength = length( AdcData );
            

            % now loop through the actual signal and make FFTs
            for ix=ObjIndexMin:(ParamHammWindowWidth/2): ObjIndexMax
                % create the desired adc window.
                % and save it into the matrix
                AdcDataWindow = double( AdcData(ix:( ix+ParamHammWindowWidth-1) ) );
                s = stft(AdcDataWindow,ParamFSampling,'OverlapLength',96,'FFTLength',ParamHammWindowWidth);
                
                MeasFileCounter     = MeasFileCounter   + 1;
                MeasFFTCounter      = MeasFFTCounter    + 1;
                ConstFftMatrix( MeasFileCounter, : ) = normalize( abs( s(ParamHammWindowWidth/2:end-1) ), 'range' );
                

                % 0 - object still not reached
                % 1 - object reached
                % 2 - object passed (reflexion)
                if( MeasFFTCounter < 3 )
                    % if index is before object
                    ConstFftVector( MeasFileCounter, 1 ) = 0;
                elseif( MeasFFTCounter < 5 )
                    % if index is over object
                    ConstFftVector( MeasFileCounter, 1 ) = 1;
                else
                    % if index is behind object
                    ConstFftVector( MeasFileCounter, 1 ) = 2;
                end

            end
            
        else
            % Current measurement is not valid (distance wrong)
            ConstInvalidMeasurements = ConstInvalidMeasurements + 1;

        end % end of one measurement ( 2^14 ADC values) inside one file

        if( Tmp_FftLengthByte > 0)
            seek(Fr,Tmp_FftLengthByte);
        end
    
        if( Tmp_FeatureLengthByte > 0 )
            seek(Fr,Tmp_FeatureLengthByte);
        end

    end % here is the end of the data set
    
end % here is the end of the file

% here is the end of all files
clearvars -except Const*;

clc;

