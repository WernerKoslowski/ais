#ifndef __SAVE_DATA_H__
#define __SAVE_DATA_H__


#ifdef __cplusplus
extern "C" {
#endif


//********************************************
// global structs
//********************************************


//********************************************
// global variables
//********************************************


//********************************************
// public functions prototypes
//********************************************
int SaveBinaryFile( uint16_t FileId, void *p_data, uint32_t ByteCount );
int DeleteFiles( void );
int SaveConfigFile(const char* ConfigFileName, float* pData, uint32_t Count );
int SaveAsciiFile( uint16_t FileId, const char* pChar );
int ReadConfigFile(const char* ConfigFileName, float* pDataOut, int ParamCount );




#ifdef __cplusplus
}
#endif
#endif
