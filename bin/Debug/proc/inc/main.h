#ifndef __MAIN_H__
#define __MAIN_H__


#ifdef __cplusplus
extern "C" {
#endif


#define THREAD_PAUSE_MS				(125)													//     200 ms = 5 Hz
#define THREAD_PAUSE_US				(THREAD_PAUSE_MS*1000)									// 100.000 µs = 100 ms (per cycle)
#define MEASURES_PER_SECOND			( 1000 / THREAD_PAUSE_MS )



//********************************************
// global structs
//********************************************


//********************************************
// global variables
//********************************************
pthread_mutex_t				run_meas_mutex;
pthread_cond_t				run_meas_cond;



#ifdef __cplusplus
}
#endif
#endif
