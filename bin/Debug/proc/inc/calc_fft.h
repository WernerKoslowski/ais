
#ifndef CALC_FFT_H
#define CALC_FFT_H


//#include "rp.h"



uint16_t calc_fft(uint32_t AdcWindowWidth, uint32_t ObjectIndex, uint32_t AdcBufferSize, int16_t *pAdcData, uint16_t *pFftData, uint16_t FftStartIndex, uint16_t FftStopIndex );


#endif

