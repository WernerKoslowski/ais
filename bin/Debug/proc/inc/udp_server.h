#ifndef __UDP_SERVER_H__
#define __UDP_SERVER_H__


#ifdef __cplusplus
extern "C" {
#endif


//********************************************
// public functions
//********************************************
void ServerThread( void *threadid );
void FillUdpDataBuffer( void *pData, uint16_t ByteCount );
void SendUdpDataBuffer( void );



#ifdef __cplusplus
}
#endif
#endif
