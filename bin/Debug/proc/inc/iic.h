#ifndef __IIC_H__
#define __IIC_H__


#ifdef __cplusplus
extern "C" {
#endif


#define SW_VERSION 0.22

//********************************************
// global functions
//********************************************
void MeasureThread( void *threadid );
void IicSetLog( int16_t SecondsToMeasure, uint8_t MeasureMode );
void IicSetRun( bool param, int16_t MeasuresCount, uint8_t MeasureMode );
void IicSetThresholdValue( float param1, float param2 );
void IicSetWindowWidth( uint32_t WindowLengthIn );
void IicSaveConfigData( void );
void IicSetAdcAutoWindowing( bool param );
void IicSetDatAndTime( const char* pDate );




#ifdef __cplusplus
}
#endif
#endif
