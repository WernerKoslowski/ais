
#ifndef FEATURES_H
#define FEATURES_H


#include "rp.h"


uint16_t InitFeatureExtraction( void );
void ConfigFeatureExtraction( float FftMinFrequencyIndexIn, float FftMaxFrequencyIndexIn, float FreqFactorIn );
float* StartFeatureExtraction( uint16_t* pDataFFT );



#endif

