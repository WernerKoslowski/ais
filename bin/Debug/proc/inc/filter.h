

#ifndef FILTER_H
#define FILTER_H

#include <stdint.h>
#include <unistd.h>
#include <float.h>
#include <fcntl.h>

#include "rp.h"

typedef struct
{
	uint16_t	idx;
	uint16_t	idx_max;
	float		latest_val;
	float		sum;
	float		filt_val;
	float		*buffer;
}Filter_t;



bool  init_filter( Filter_t *f, uint16_t count_in );
float run_mean_filter( Filter_t *f, float *val_in );
float run_diff_filter( Filter_t *f, float *val_in );
float run_variance_filter( Filter_t *f);


#endif