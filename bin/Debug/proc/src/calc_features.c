// ************************************************************ //
// ******************** includes ****************************** //
// ************************************************************ //
#include <fcntl.h>

#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <stdint.h>
#include <float.h>

#include "calc_features.h"
#include "filter.h"



// ************************************************************ //
// ******************** defines ******************************* //
// ************************************************************ //
#define F1_FILTER_WIDTH				10
#define F2_FILTER_WIDTH				10
#define F6_FILTER_WIDTH				10
#define F9_FILTER_WIDTH				10





// ************************************************************ //
// ******************** private variables ********************* //
// ************************************************************ //
static Filter_t				FilterF1;
static Filter_t				FilterF3;
static Filter_t				FilterF6;
static Filter_t				FilterF9;
static float				Features[10] = {0};
static float				FftFreqFactor;
static float				FftMinFrequency;
static float				FftMinFrequencyIndex;
static float				FftCenterFrequency;
static float				FftCenterFrequencyIndex;
static float				FftMaxFrequency;
static float				FftMaxFrequencyIndex;
static float				FftFrequencyIndexDiff;



// ************************************************************ //
// ************* static/private function prototypes *********** //
// ************************************************************ //
static void CalcFeatureF1( void );
static void CalcFeatureF2( uint16_t *data );
static void CalcFeatureF3( void );
static void CalcFeatureF4( void );
static void CalcFeatureF5( uint16_t *data );
static void CalcFeatureF6( void );
static void CalcFeatureF7( void );
static void CalcFeatureF8( uint16_t *data );
static void CalcFeatureF9( void );
static void CalcFeatureF10( void );



// ************************************************************ //
// ******************** public functions ********************** //
// ************************************************************ //
uint16_t InitFeatureExtraction( void )
{
	init_filter( &FilterF1, F1_FILTER_WIDTH);
	init_filter( &FilterF3, F2_FILTER_WIDTH);
	init_filter( &FilterF6, F6_FILTER_WIDTH);
	init_filter( &FilterF9, F9_FILTER_WIDTH);
	
	return( (uint16_t)( sizeof(Features)/sizeof(Features[0] ) ) );
}



void ConfigFeatureExtraction( float FftMinFrequencyIndexIn, float FftMaxFrequencyIndexIn, float FreqFactorIn )
{
	FftMinFrequencyIndex	= FftMinFrequencyIndexIn;
	FftMaxFrequencyIndex	= FftMaxFrequencyIndexIn;
	FftFreqFactor			= FreqFactorIn;
	
	FftMinFrequency			= FftMinFrequencyIndex * FftFreqFactor;
	FftMaxFrequency			= FftMaxFrequencyIndex * FftFreqFactor;
	FftFrequencyIndexDiff	= FftMaxFrequencyIndex - FftMinFrequencyIndex + 1.0;
}



float* StartFeatureExtraction( uint16_t* pDataFFT )
{
	// get F2 (peak frequency)
	CalcFeatureF2( pDataFFT );
	
	// get F1 (mean difference over 10 times F2 )
	CalcFeatureF1( );
	
	// get F3 (mean over 10 times F2)
	CalcFeatureF3( );
	
	// get F4 (variance over 10 times F2)
	CalcFeatureF4( );
	
	// get F5 (count of peaks within range of  39.5 kHz to 41.5 kHz)
	CalcFeatureF5( pDataFFT );
	
	// get F6 (mean over 10 times F5)
	CalcFeatureF6( );
	
	// get F7 (variance over 10 times F5)
	CalcFeatureF7( );
	
	// get F8 (mean distance of 2 peaks (left/right) beneth F2)
	CalcFeatureF8( pDataFFT );
	
	// get F9 (mean over 10 times F8)
	CalcFeatureF9(  );
	
	// get F10 (variance over 10 times F8)
	CalcFeatureF10( );
	
	return( (float *)&Features[0] );
}



float* GetFeaturePointer( void )
{
	return( Features );
}




// ************************************************************ //
// ******************** private functions ********************* //
// ************************************************************ //

// get F2 ( maximum frequency amplitude )
static void CalcFeatureF2( uint16_t *FftData )
{
	uint16_t MaxVal=0;
	FftCenterFrequencyIndex = 0.0;
	
	for( uint16_t ix = 0; ix<= (uint16_t)FftFrequencyIndexDiff; ix++ )
	{
		if( FftData[ix] > MaxVal ){
			MaxVal 					= FftData[ix];
			FftCenterFrequencyIndex	= (float)ix;
		}
	}
	
	
	FftCenterFrequencyIndex += FftMinFrequencyIndex;
	FftCenterFrequency	= FftCenterFrequencyIndex * FftFreqFactor;
	Features[1]			= FftCenterFrequency;
}



// get F1 (mean difference over 10 times F2 )
static void CalcFeatureF1( void )
{
	Features[0] = run_diff_filter( &FilterF1, &Features[1] );
}



// get F3 (mean over 10 times F2)
static void CalcFeatureF3( void )
{
	Features[2] = run_mean_filter( &FilterF3, &Features[1] );
}



// get F4 (variance over 10 times F2)
static void CalcFeatureF4( void )
{
	Features[3] = run_variance_filter( &FilterF3 );
}



// get F5 (number of peaks between 39.5 and 41.5 kHz )
static void CalcFeatureF5( uint16_t *FftData )
{
	uint16_t MinIndex = (uint16_t)( 39500.0 / FftFreqFactor - FftMinFrequencyIndex );
	uint16_t MaxIndex = (uint16_t)( 41500.0 / FftFreqFactor - FftMinFrequencyIndex );
	uint16_t cnt = 0;
	for( uint16_t ix=MinIndex; ix <= MaxIndex; ix++ ){
		if( (FftData[ix] < FftData[ix+1]) && (FftData[ix+1] > FftData[ix+2]) ){
			cnt++;
		}
	}
	
	Features[4] = (float)cnt;
}



// get F6 (mean over 10 times F5)
static void CalcFeatureF6( void )
{
	Features[5] = run_mean_filter( &FilterF6, &Features[4] );
}



// get F7 (variance over 10 times F5)
static void CalcFeatureF7( void )
{
	Features[6] = run_variance_filter( &FilterF6 );
}



// get F8 (mean distance of 2 peaks (left/right) beneth F2)
static void CalcFeatureF8( uint16_t *data )
{
	uint16_t c_ix 		= (uint16_t)(FftCenterFrequencyIndex - FftMinFrequencyIndex);
	
	uint16_t *p1, *p2;
	p1 = p2 = &data[ c_ix ];
	float dif1, dif2;
	bool flag1=false, flag2=false;
	dif1 = dif2 = 0.0;
	
	for( uint16_t ix = 0; ix < ( (uint16_t)FftFrequencyIndexDiff-2)/2; ix++ )
	{
		
		if( !flag2 ){
			p2++;
			if( ( *(p2+1) > *p2 ) && ( *(p2+2) < *(p2+1)  ) ){
				// next maximum found above/right from center frequency
				flag2 ^= true;
				dif2 = ( (float)c_ix + (float)(ix+1.0) ) * FftFreqFactor - FftCenterFrequency;
			}
			
		}
		
		if( !flag1 ){
			p1--;
			if( ( *(p1-1) > *p1 ) && ( *(p1-2) < *(p1-1)  ) ){
				// next maximum found under/left from center frequency
				flag1 ^= true;
				dif1 = FftCenterFrequency - ( (float)c_ix - (float)(ix+1.0) ) * FftFreqFactor;
			}
		}
		if( flag1 && flag2 ){break;}
	}
	Features[7] = (dif1 + dif2)/2;
	//Features[7] = FftCenterFrequencyIndex;//(dif1 + dif2) / 2;
}



// get F9 (mean over 10 times F8)
static void CalcFeatureF9( void )
{
	Features[8] = run_mean_filter( &FilterF9, &Features[7] );
}



// get F10 (variance over 10 times F8)
static void CalcFeatureF10( void )
{
	Features[9] = run_variance_filter( &FilterF9 );
}

