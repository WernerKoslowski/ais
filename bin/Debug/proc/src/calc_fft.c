// ************************************************************ //
// ******************** includes ****************************** //
// ************************************************************ //
#include <fcntl.h>

#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <float.h>
#include "_kiss_fft_guts.h"
#include "kiss_fft.h"
#include "kiss_fftr.h"
#include "calc_fft.h"

// ************************************************************ //
// ******************** defines ******************************* //
// ************************************************************ //




// ************************************************************ //
// ******************** private variables ********************* //
// ************************************************************ //




// ************************************************************ //
// ************* static/private function prototypes *********** //
// ************************************************************ //



// ************************************************************ //
// ******************** public functions ********************** //
// ************************************************************ //



/*!
*
* @brief		This function executes the FFT for given ADC values
* 
* @details		The function needs a signed integer 16 bit pointer to an array of the length <length>
*				and a pointer to an array where the result(s) will be stored.
*
* @param		uint32_t AdcWindowWidth:	the length of data, which will be analized
* @param		uint32_t ObjectIndex: 		position as index where object is located
* @param		uint32_t AdcBufferSize: 	size of ADC buffer (how many samples to be anlized)
*
*/
uint16_t calc_fft(uint32_t AdcWindowWidth, uint32_t ObjectIndex, uint32_t AdcBufferSize, int16_t *pAdcData, uint16_t *pFftData, uint16_t FftStartIndex, uint16_t FftStopIndex )
{
	// create an output buffer for FFT
	// create a config variable for FFT
	 int16_t *pAdcDataIn;
	kiss_fftr_cfg	FftConfig;
	
	kiss_fft_cpx *FftRawData	= (kiss_fft_cpx *)calloc( (AdcWindowWidth) , sizeof(kiss_fft_cpx));
	uint32_t 	*FftBuffer		= calloc( AdcWindowWidth, sizeof(uint32_t) );
	float		re, im;
	float		fMaxAmplitude = 0.0;
	uint16_t	MaxIndex;
	uint16_t	MinIndex;
	
	
	if( ObjectIndex < AdcWindowWidth/2 ){
		// object located "left" in signal
		MinIndex	= 0;
	}
	else if(ObjectIndex > ( AdcBufferSize - AdcWindowWidth/2)){
		// object located "right" in signal
		MinIndex	= AdcBufferSize - AdcWindowWidth;
	}
	else{
		// object located around "mid" of signal
		MinIndex	= ObjectIndex - AdcWindowWidth/2;
	}
	
	// here the starting pointer for the FFT is set
	pAdcDataIn 	= &pAdcData[ MinIndex ];
	
	// allocate temporary space for calculation
	FftConfig = kiss_fftr_alloc(AdcWindowWidth,0,NULL,NULL);
	
	
	// execute the FFT
	kiss_fftr( FftConfig, (kiss_fft_scalar *) pAdcDataIn, FftRawData );
	
	
	// build the sum of imaginary and real part of the FFT
	// and search the MaxAmplitude
	for( uint16_t ix = 0; ix <= (FftStopIndex - FftStartIndex + 1); ix++ ){
		re = (float)FftRawData[ix + FftStartIndex].r, im = (float)FftRawData[ix + FftStartIndex].i;
		FftBuffer[ix] = (uint32_t)(sqrtf( re*re + im*im ));
		if( (float)FftBuffer[ix] > fMaxAmplitude ){
			fMaxAmplitude = (float)FftBuffer[ix];
			MaxIndex = (uint16_t)ix;
		}
	}
	
	fMaxAmplitude /= 1000.0;
	
	// normalize data
	for( uint16_t ix = 0; ix <= ( AdcWindowWidth/2 ); ix++ ){
		pFftData[ix] = (uint16_t)( ((float)FftBuffer[ix]) / fMaxAmplitude );
	}
	
	free( FftRawData );
	free( FftConfig );
	free( FftBuffer );
	
	return( MaxIndex );
}




// ************************************************************ //
// ******************** private functions ********************* //
// ************************************************************ //
