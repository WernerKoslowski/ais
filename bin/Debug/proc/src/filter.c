
#include "filter.h"
#include <stdlib.h>




// ************************************************************ //
// ******************** Filter functions ********************** //
// ************************************************************ //
bool init_filter( Filter_t *f, uint16_t count_in )
{
	if( f != NULL ){
		f->idx_max = count_in;
		f->idx		= 0;
		f->sum		= 0;
		f->buffer	= calloc( count_in, sizeof(float) );
		return(0);
	}
	return(-1);
}



// universal running average filter for x values
float run_mean_filter( Filter_t *f, float *val_in )
{
	f->sum -= f->buffer[ f->idx ] - *val_in;
	f->buffer[ (f->idx)++ ] = *val_in;
	f->idx %= f->idx_max;
	f->filt_val = (float)(f->sum / f->idx_max);
	return( f->filt_val );
}



// universal mean diff filter for x values
float run_diff_filter( Filter_t *f, float *val_in )
{
	uint32_t new_diff = abs( f->latest_val - *val_in );
	f->sum +=  new_diff - f->buffer[f->idx];
	f->buffer[f->idx++] = new_diff;
	f->idx %= f->idx_max;
	f->latest_val = *val_in;
	f->filt_val = (float)( f->sum / f->idx_max);
	return( f->filt_val );
}



// universal variance filter for x values
float run_variance_filter( Filter_t *f)
{
	float sum=0;
	// build sum of the square of the differences between
	// single and mean value
	for( uint8_t ix=0; ix<f->idx_max; ix++ )
	{
		sum += (f->buffer[ix] - f->filt_val) * (f->buffer[ix] - f->filt_val);
	}
	// divide the sum by the count of measurements
	return( (float)(sum / (float)f->idx_max) );
}





