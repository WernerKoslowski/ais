#include <arpa/inet.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <stdint.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <sys/time.h>

#include "rp.h"
#include "udp_server.h"
#include "iic.h"

// daemon includes
#include <pthread.h>
#include <syslog.h>


#define UDP_PORT		61231


//********************************************
// private structs
//********************************************
typedef struct
{
	int			socket;
	socklen_t 	length;
	char		command;
	struct		sockaddr_in serveraddr, clientaddr;
	int32_t		parameter[4];
}udp_t;



//********************************************
// extern variables
//********************************************



//********************************************
// local variables
//********************************************
static udp_t				udp={-1,-1};
static volatile uint8_t		pDataBuffer[ 64 * 1024 ];
static volatile uint32_t	BytesToTransfer = 0;



//********************************************
// local function prototypes
//********************************************
static void parse_udp_message( char *msg_rx );



//********************************************
// public functions
//********************************************
// Thread No. 1 for UDP!
/*
*	@brief		Thread waits for UDP message(s) in blocked mode
*				and parses the received message(s) and value(s).
*	@name		ServerThread
*/
void ServerThread( void *threadid )
{
	char rx_buffer[40];
	memset( &udp.serveraddr,	0, sizeof(udp.serveraddr) );
	memset( &udp.clientaddr,	0, sizeof(udp.clientaddr) ); 
	
	udp.serveraddr.sin_family = AF_INET;
	udp.serveraddr.sin_port = htons( UDP_PORT );
	udp.serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
	
	udp.length = sizeof(udp.clientaddr);
	
	// Create a socket with UDP protocol
	udp.socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	bind( udp.socket, (struct sockaddr *)&udp.serveraddr, sizeof(udp.serveraddr));
	
	while( 1 ){
		memset( (void *)rx_buffer, (int)0, (size_t)(strlen( rx_buffer )) );
		// Go into blocked mode to free CPU; wait for UDP message
		if( recvfrom( udp.socket, rx_buffer, sizeof(rx_buffer), 0, (struct sockaddr *)&udp.clientaddr, &udp.length ) < 0 ){
			break;
		}
		
		parse_udp_message( rx_buffer );
		
	} // end while()
	
	close( udp.socket );
	pthread_exit(NULL);
}



void FillUdpDataBuffer( void *pData, uint16_t ByteCount )
{
	memcpy( (void *)&pDataBuffer[ BytesToTransfer ], (const void *)pData, (size_t) ByteCount );
	BytesToTransfer += ByteCount;
}



void SendUdpDataBuffer( void )
{
	while( BytesToTransfer > 0 )
	{
		BytesToTransfer -= sendto( udp.socket, (uint8_t *)pDataBuffer, BytesToTransfer, MSG_DONTWAIT,  (struct sockaddr *) &udp.clientaddr, udp.length);
	}
}







//********************************************
// local functions
//********************************************
static void parse_udp_message( char *msg_rx )
{
	char *rx_param;
	
	if( msg_rx[0] == '-' ){
		
		udp.command = msg_rx[1];
		if( (rx_param = strstr( msg_rx, " ")) != NULL ){
			udp.parameter[0] = atoi( rx_param++ );
			if( (rx_param = strstr( rx_param, " ")) != NULL ){
				udp.parameter[1] = atoi( rx_param++ );
				if( (rx_param = strstr( rx_param, " ")) != NULL){
					udp.parameter[2] = atoi( rx_param++ );
				}
			}
		}
		
		switch( udp.command )
		{
			case 'c':
				IicSaveConfigData();
				break;
			case 'l':
				IicSetLog( (int16_t)udp.parameter[0], (int16_t)udp.parameter[1]);
				break;
			case 'r':
				IicSetDatAndTime( (const char *)&msg_rx[3] );
				break;
			case 's':
				IicSetRun( (udp.parameter[0] == 1), (int16_t)udp.parameter[1], (uint8_t)udp.parameter[2] );
				break;
			case 't':
				// change threshold values for boxclassifier
				IicSetThresholdValue( (float)( abs(udp.parameter[0]) ), (float)( abs(udp.parameter[1]) ) );
				break;
			case 'v':
				IicSetAdcAutoWindowing( (udp.parameter[0] == 1) );
				break;
			case 'w':
				IicSetWindowWidth( (uint32_t)(abs(udp.parameter[0])) );
				break;
			case 'x':
				// restart the program
				
				break;
			default:
				break;
		} // end switch udp.command
	} // end if '-'
	
}





