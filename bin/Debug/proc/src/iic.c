
/* This code is sued for starting continous ranging on SRF02 sensor
 * 
 * (c) Red Pitaya  http://www.redpitaya.com
 *
 * This part of code is written in C programming language.
 * Please visit http://en.wikipedia.org/wiki/C_(programming_language)
 * for more details on the language used herein.
 */
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <stdint.h>
#include <float.h>
#include <sys/time.h>

// daemon includes
#include <pthread.h>
#include <syslog.h>

#include "rp.h"
#include "_kiss_fft_guts.h"
#include "kiss_fft.h"
#include "kiss_fftr.h"
#include "main.h"
#include "iic.h"
#include "udp_server.h"
#include "save_data.h"
#include "nn.h"
#include "i2c.h"
#include "filter.h"
#include "calc_features.h"
#include "calc_fft.h"


#define DATA_CONFIG_FILE			"/root/iic/ConfigFile.conf"


//	global defines
#define	V_SONIC_WAVE				(float)(343.2)													// [m/s] Schallgeschwindigkeit in Luft

#define ADC_MAX_SAMPLE_FREQUENCY	125000000														// [Hz] --> 125 MHz
#define ADC_SAMPLE_DECIMATION		64																// [-]
#define ADC_SAMPLE_FREQUENCY		( ADC_MAX_SAMPLE_FREQUENCY / ADC_SAMPLE_DECIMATION )
#define ADC_SAMPLE_TIME				8																// [ns]
#define ADC_SAMPLE_TIME_NS			(uint32_t)( ADC_SAMPLE_DECIMATION * ADC_SAMPLE_TIME )			// [ns] --> 8*64=512 ns / sample
#define	ADC_START_DELAY_US			(uint32_t)( 0.30 * 2 * 1e6 / V_SONIC_WAVE )						// [µs] --> 2 * 0,30 m / 343,2 m/s = 1.748 µs
#define ADC_BUFFER_DELAY_US			(uint32_t)(( ADC_BUFFER_SIZE * ADC_SAMPLE_TIME_NS ) / (1e3))	// [µs] --> (16.384 * 512 ns ) / 1000 = 8.388 µs
#define ADC_MID_US					(ADC_START_DELAY_US + ( ADC_BUFFER_DELAY_US / 2 ))

#define FFT_MIN_FREQ				35000
#define FFT_MAX_FREQ				45000															// [Hz]
#define FFT_WINDOW_STD				8192															// FFT window width in samples [-]

#define LEDx_INIT()					rp_DpinSetDirection( RP_LED0, RP_OUT ), \
									rp_DpinSetDirection( RP_LED1, RP_OUT ), \
									rp_DpinSetDirection( RP_LED2, RP_OUT ),	\
									rp_DpinSetDirection( RP_LED3, RP_OUT ), \
									rp_DpinSetDirection( RP_LED4, RP_OUT ), \
									rp_DpinSetState( RP_LED0, RP_LOW ),		\
									rp_DpinSetState( RP_LED1, RP_LOW ),		\
									rp_DpinSetState( RP_LED2, RP_LOW ),		\
									rp_DpinSetState( RP_LED3, RP_LOW ),		\
									rp_DpinSetState( RP_LED4, RP_LOW )


#define __LED_OFF(X)					rp_DpinSetState( RP_LED##X, RP_LOW  )
#define __LED_ON(X)					rp_DpinSetState( RP_LED##X, RP_HIGH )

#define LED0_ON						__LED_ON(0)
#define LED0_OFF					__LED_OFF(0)
#define LED1_ON						__LED_ON(1)
#define LED1_OFF					__LED_OFF(1)
#define LED2_ON						__LED_ON(2)
#define LED2_OFF					__LED_OFF(2)
#define LED3_ON						__LED_ON(3)
#define LED3_OFF					__LED_OFF(3)



typedef enum
{
	ERR_NO				=  0,
	ERR_LOGIC			= -1,
	ERR_FILE_CREATION	= -2,
	ERR_FILE_OPEN		= -3,
	ERR_DATA			= -4,
	ERR_COM				= -5,
	ERR_I2C				= -6,
	ERROR_FILE			= -7,
	ERR_TCP				= -8,
	ERR_FATAL			= -127
}error_t;



typedef struct
{
	// values to be read out or set
	float VelocitySonicWave;
	float AdcMaxSamplingFrequency;
	float AdcSamplingDecimation;
	float AdcSampleCount;
	float AdcBitDepth;
	float AdcWindowLength;
	float AdcWindowOffset;
	float MinimumSensorDistance;
	float FftMinFrequency;
	float FftMaxFrequency;
	float AdcAutoWindowing;
	
	// values to calculate
	float AdcResultingSamplingFrequency;
	float AdcStartDelayMicroSeconds;
	float FftFrequencyFactor;
	float FftSampleCount;
	float AdcBufferFillDelayMicroSeconds;
	float FftMinFrequencyIndex;
	float FftMaxFrequencyIndex;
	float FeatureDataLength;
}ConfigData_t;



typedef struct
{
	uint16_t	w_start;
	uint16_t	object_ix;
	uint16_t	start_delay_us;
	uint32_t	buffer_size;
}adc_t;




// 19 x 4 byte = 76 bytes
typedef struct
{
	float HeaderLength;
	float AdcDataLength;
	float FftDataLength;
	float FeatureDataLength;
	float Class;
	float DataType;
	float X_Interval;
	float Scaling;
	float SampleFrequency;
	float ADCResolution;
	float AmbTemperature;
	float MeasDelay;
	float Distance;
	float FFTWindowLength;
	float FFTWindowOffsetIndex;
	float SWVersion;
	float reserved1;
	float reserved2;
	float TimeStamp;
	// Index for beginning of FFT window in time domain
	
}Header_t;



typedef struct
{
	 int16_t pAdcData[ADC_BUFFER_SIZE];
	uint16_t pFftData[ADC_BUFFER_SIZE/2];
}Data_t;



// global constants
static const uint16_t WindowWidths[] = { 64, 128, 256, 512, 1024, 2048, 4096, 8192 };


// global variables definition
static Filter_t					filter_distance;
static volatile ConfigData_t	ConfigData	= { 0 };
static volatile uint16_t		FileId = 0;

// variable s for I/O
static float				time_of_flight_us;
static Header_t				Header;
static Data_t				Data;
static adc_t				adc;
static uint16_t				CountMeasuresToSave;			// max: 10.000 measurements --> 1000 seconds ( 10 measurements / file )
static float				F1_threshold	=   140;
static float				F10_threshold	= 10000;
static volatile bool		Run			= false;
static volatile int16_t		RunCount	= -1;
static volatile uint8_t		RunMode		=  0;
static volatile bool		Log			= false;



void AdcInit(void)
{
	rp_Init();
	rp_DpinSetDirection( RP_DIO0_P, RP_IN);							// DIO0 as input
	rp_DpinSetState( RP_DIO0_P, RP_LOW);							// DIO0 set to low
	//rp_AcqSetDecimation( RP_DEC_64 );								// Decimation 64 --> 64 * 8 ns = 512 ns / sample
	rp_AcqSetSamplingRate( RP_SMP_1_953M );							// Sample rate 1.953Msps; Buffer time length 8.388ms; Decimation 64
	rp_AcqSetArmKeep( false );
	rp_AcqSetTriggerSrc( RP_TRIG_SRC_DISABLED );					// Trigger disabled
	rp_AcqSetTriggerDelay( 0 );
	rp_AcqSetAveraging(true);
	
	// set the length of the buffer for ADC
	adc.buffer_size			= (uint32_t)ADC_BUFFER_SIZE;		// 16.384
}




/*
* @brief		This function sets all physical variables for runtime.
*
* @details		This function can be called with a reference pointer to
*				new physical values.
*				!!! Be Careful, the values are not proofed !!! 
*
* @param[in]	pData - pointer to 1-D single precision vector - can be NULL
*				to set all default values.
*
*/
void InitConfigData( float* pData )
{
	// values to read out
	ConfigData.VelocitySonicWave			= ( pData == NULL )?(          342.903824):( *(pData++) );		// [m/s]
	ConfigData.AdcMaxSamplingFrequency		= ( pData == NULL )?(    125000000.000000):( *(pData++) );		// [MHz]
	ConfigData.AdcSamplingDecimation		= ( pData == NULL )?(           64.000000):( *(pData++) );		// [-]
	ConfigData.AdcSampleCount				= ( pData == NULL )?(     ADC_BUFFER_SIZE):( *(pData++) );		// 2^14 Samples (received from rp.h)
	ConfigData.AdcBitDepth					= ( pData == NULL )?(			12.000000):( *(pData++) );		// ADC resolution (12 bit)
	ConfigData.AdcWindowLength				= ( pData == NULL )?(         8192.000000):( *(pData++) );		// [-]
	ConfigData.AdcWindowOffset				= ( pData == NULL )?(            0.000000):( *(pData++) );
	ConfigData.MinimumSensorDistance		= ( pData == NULL )?(            0.300000):( *(pData++) );		// minimum distance to sensor
	ConfigData.FftMinFrequency				= ( pData == NULL )?(        35000.000000):( *(pData++) );		// f = 35 kHz
	ConfigData.FftMaxFrequency				= ( pData == NULL )?(        45000.000000):( *(pData++) );		// f = 45 kHz
	ConfigData.AdcAutoWindowing				= ( pData == NULL )?(        	 1.000000):( *(pData++) );		// 0.0 or != 0.0 (bools variable)
}



void CalcConfigData( void )
{
	
	// values to calculate
	ConfigData.AdcResultingSamplingFrequency	= ConfigData.AdcMaxSamplingFrequency / ConfigData.AdcSamplingDecimation;
	ConfigData.AdcStartDelayMicroSeconds		= (2e6 * ConfigData.MinimumSensorDistance) / ConfigData.VelocitySonicWave;
	ConfigData.FftFrequencyFactor				= ( ConfigData.AdcResultingSamplingFrequency / ConfigData.AdcSampleCount ) / 2 * ( ConfigData.AdcSampleCount / ConfigData.AdcWindowLength );
	ConfigData.AdcBufferFillDelayMicroSeconds	= (1e6 * ConfigData.AdcSampleCount) * ( 1 / ConfigData.AdcResultingSamplingFrequency );
	ConfigData.FftMinFrequencyIndex				= ConfigData.FftMinFrequency / ConfigData.FftFrequencyFactor;
	ConfigData.FftMaxFrequencyIndex				= ConfigData.FftMaxFrequency / ConfigData.FftFrequencyFactor;
	ConfigData.FftSampleCount					= ceil( (float)(ConfigData.FftMaxFrequencyIndex - ConfigData.FftMinFrequencyIndex + 1.0) );
}



/*
* @brief	This function is called, if ADC data has to be generated
*
* @details	The function calculates time differences, which are needed for 
*			starting and stopping the measurement depending on needed distance. 
* 			
*
*
*/
int StartAdc( adc_t *a, int16_t *data )
{
	char SensorData[2];
	
	I2CStart();
	
	// wait for sonic to pass by 
	// the minimum distance
	usleep( ConfigData.AdcStartDelayMicroSeconds + a->start_delay_us );
	
	// ADC is started
	rp_AcqStart();
	
	// wait until buffer is full
	usleep( (int)ConfigData.AdcBufferFillDelayMicroSeconds );
	
	// ADC is stopped
	rp_AcqStop();
	
	// get data out of ADC buffer.
	rp_AcqGetLatestDataRaw( RP_CH_1, &a->buffer_size, data );
	
	do{
		usleep(125);
	}while( !I2CReadSensorData( &SensorData[0] ) );
	
	
	time_of_flight_us = (float)( SensorData[1]<<8 | SensorData[2] );
	
	// active auto windowing feature
	if( (ConfigData.AdcAutoWindowing >= 1.0) && ( time_of_flight_us > ADC_MID_US ) ){
		a->start_delay_us = (time_of_flight_us - ADC_MID_US);
	}
	else{
		a->start_delay_us = 0;
	}
	
	// the index of the discovered object is calculated by percentaged delay 
	// between time-of-flight and adc start delay, multiplied by the count of adc buffer
	a->object_ix = (uint16_t)( (uint32_t)( ((time_of_flight_us - ( ConfigData.AdcStartDelayMicroSeconds + a->start_delay_us)) * a->buffer_size) / ConfigData.AdcBufferFillDelayMicroSeconds ) );
	
	return(0);
}






// Thread No. 2 for measuring!
/*
*	@brief		Thread waits for signal from ServerThread in blocked mode
*				and processes the udp.command(s). 
*	@name		MeasureThread
*/
void MeasureThread( void *threadid )
{
	// variables for FFT and measurement
	uint32_t				counter_ms			= 0;
	float					Distance;
	float*					pConfigData;
	float*					pFeatureData = NULL;
	time_t 					TimeVal;
	
	
	 int16_t *pAdc = (  int16_t *)&(Data.pAdcData[0]);
	uint16_t *pFft = ( uint16_t *)&(Data.pFftData[0]);
	
	IicSetDatAndTime("2022-10-10 00:00");
	
	pConfigData = calloc( sizeof( ConfigData_t ) / sizeof( float ), sizeof( float ) );
	
	/******************************/
	// Init Functions
	/******************************/
	AdcInit();
	
	
	// init all LEDs (LED0 - LED3)
	LEDx_INIT();
	
	
	// init all features in calc_features.c
	// and get back the amount of features
	ConfigData.FeatureDataLength = (float)(InitFeatureExtraction( ));
	
	
	// Open I²C port for reading and writing
	I2CInit();
	
	
	init_filter( &filter_distance, 6 );
	
	
	// read out config file
	( ReadConfigFile( DATA_CONFIG_FILE, pConfigData, sizeof(ConfigData_t)/sizeof(float) ) > 0 ) ? ( InitConfigData( pConfigData ) ) : ( InitConfigData( NULL ) );
	
	// calculate the rest of the values
	CalcConfigData();
	
	
	// now free the RAM again
	free( pConfigData );
	
	
	Header.HeaderLength 	= (float)( sizeof( Header ) );
	Header.AdcDataLength	= (float)( sizeof( Data.pAdcData ) );
	Header.FftDataLength	= (float)( sizeof( Data.pFftData ) );
	Header.SampleFrequency	= (float)( ConfigData.AdcResultingSamplingFrequency );
	Header.ADCResolution	= (float)( ConfigData.AdcBitDepth );
	Header.SWVersion		= (float)SW_VERSION;
	
	// While loop - stay here while in "normal" operation
	while( 1 )
	{
		( Run == true )?( LED1_ON ):( LED1_OFF );
		( CountMeasuresToSave > 0 )?( LED2_ON ):( LED2_OFF );
		
		
		// increment a counter; this is the time-base for all messurements and outputs [ms]
		counter_ms = (counter_ms + THREAD_PAUSE_MS) % 1500;
		
		
		// (re-)calculate all config data
		CalcConfigData();
		
		
		// (re-)configure the feature extraction
		ConfigFeatureExtraction( ConfigData.FftMinFrequencyIndex, ConfigData.FftMaxFrequencyIndex, ConfigData.FftFrequencyFactor );
		
		
		// wait for signal from TimeThread (get signal every 100 ms)
		// thread will be set into blocked mode
		pthread_cond_wait(&run_meas_cond, &run_meas_mutex);
		
		// get current time stamp
		TimeVal = time(NULL);
		
		
		StartAdc( (adc_t *)&adc, (int16_t *)pAdc );
		Distance = ( ConfigData.VelocitySonicWave * (time_of_flight_us / 2e6) );
		
		// calculate the mean of the distance over x measurements
		Header.Distance = run_mean_filter( &filter_distance, &Distance );
		
		// proof, if a minimum of distance to the object is measured
		if( Header.Distance > ConfigData.MinimumSensorDistance )
		{
			LED0_ON;
			
			Header.AdcDataLength 		= ( RunMode & 0x01 )?( ConfigData.AdcSampleCount 	* sizeof(uint16_t) 	):( 0.0 );
			Header.FftDataLength 		= ( RunMode & 0x02 )?( ConfigData.FftSampleCount 	* sizeof( int16_t) 	):( 0.0 );
			Header.FeatureDataLength 	= ( RunMode & 0x04 )?( ConfigData.FeatureDataLength * sizeof(float) 	):( 0.0 );
			
			// start the FFT and get the maximum amplitude position back
			calc_fft( (uint32_t)ConfigData.AdcWindowLength, (uint32_t)adc.object_ix, (uint32_t)adc.buffer_size, (int16_t *)pAdc, (uint16_t *)pFft, (uint16_t)ConfigData.FftMinFrequencyIndex, (uint16_t)ConfigData.FftMaxFrequencyIndex );
			
			pFeatureData = StartFeatureExtraction( (uint16_t *)pFft );
			
			// try to identify a soft / hard object_class
			Header.Class 					= (float)(  ( (pFeatureData[0] > F1_threshold) && (pFeatureData[9] > F10_threshold) )?(1):(0)  );
			Header.X_Interval				= (float)( ConfigData.FftFrequencyFactor );
			Header.MeasDelay				= (float)( adc.start_delay_us );
			Header.FFTWindowLength 			= (float)( ConfigData.AdcWindowLength / 2.0 );
			Header.FFTWindowOffsetIndex 	= (float)( ConfigData.FftMinFrequencyIndex );
			Header.DataType					= (float)( RunMode );
			
			// fill timestamp into Header
			Header.TimeStamp = (float)( (uint32_t)TimeVal % (uint32_t)86400 );
			
			if( Run && (RunCount !=  0) )
			{
				
				if( RunCount > 0 ){
					if( --RunCount == 0 ){
						Run ^= true;
					}
				}
				
				
				
				
				// fill UDP data buffer with header information
				FillUdpDataBuffer( (void *)&Header, (uint16_t)Header.HeaderLength );
				
				// fill databuffer with ADC, FFT and/or feature Data if requested
				if( RunMode & 0x01 ){
					FillUdpDataBuffer( (void *)pAdc, (uint16_t)Header.AdcDataLength );
				}
				if( RunMode & 0x02 ){
					FillUdpDataBuffer( (void *)pFft, (uint16_t)Header.FftDataLength );
				}
				if( RunMode & 0x04 ){
					if( pFeatureData != NULL )
					{
						FillUdpDataBuffer( (void *)pFeatureData,	(uint16_t)Header.FeatureDataLength );
					}
					
				}
				
				SendUdpDataBuffer();
				
			}
			
			
			if( CountMeasuresToSave > 0 )
			{
				SaveBinaryFile( FileId, (void *)&Header, 		 (uint32_t)(Header.HeaderLength) );
				
				if( RunMode & 0x01 ){
					SaveBinaryFile( FileId, (void *)pAdc,		 (uint32_t)(Header.AdcDataLength) );
				}
				if( RunMode & 0x02 ){
					SaveBinaryFile( FileId, (void *)pFft,		 (uint32_t)(Header.FftDataLength) );
				}
				if( RunMode & 0x04 ){
					SaveBinaryFile( FileId,(void *)pFeatureData, (uint32_t)(Header.FeatureDataLength) );
				}
				
				if( (--CountMeasuresToSave) == 0 ){
					(LED2_OFF);
					FileId++;
				}
			}
			
		}
		else
		{
			LED0_OFF;
		}
		
	} // end while(1)
	
	
	rp_Release();
	
	pthread_exit(NULL);
}



//********************************************
// public functions - used by UDP-Client
//********************************************
void IicSetLog( int16_t SecondsToMeasure, uint8_t MeasureMode )
{
	if( SecondsToMeasure < 0 )
	{
		DeleteFiles();
		LED2_OFF;
	}
	else if( SecondsToMeasure <= 600 )		// maximum 600 seconds -> 10 Minutes
	{
		CountMeasuresToSave = SecondsToMeasure *  MEASURES_PER_SECOND;
		LED2_ON;
	}
	
	RunMode 	= MeasureMode;
	Run 		= false;
}



void IicSetRun( bool param, int16_t MeasuresCount, uint8_t MeasureMode )
{
	Run			= param;
	RunCount	= (MeasuresCount >= -1 )?( MeasuresCount * MEASURES_PER_SECOND ):( -1 );
	RunMode		= MeasureMode;
}



void IicSetThresholdValue( float param1, float param2 )
{
	F1_threshold	= param1;
	F10_threshold	= param2;
}



void IicSetWindowWidth( uint32_t AdcWindowLengthIn )
{
	ConfigData.AdcWindowLength = (float)AdcWindowLengthIn;
}



void IicSaveConfigData( void )
{
	SaveConfigFile( DATA_CONFIG_FILE, (float *)&ConfigData, sizeof(ConfigData)/sizeof(float) );
}



void IicSetAdcAutoWindowing( bool param )
{
	ConfigData.AdcAutoWindowing = ((param)?(1.0):(0.0));
}



void IicSetDatAndTime( const char* pDate )
{
	char Date[35];
	sprintf(Date, "date --set='%s'", pDate);
	system( Date );
}


