
#include "rp.h"
#include <linux/i2c-dev.h>
#include <unistd.h>
#include "i2c.h"
#include <fcntl.h>

//	global defines
#define I2C_SLAVE_FORCE 		   	0x0706
#define I2C_SLAVE    			   	0x0703    /* Change slave address            */
#define I2C_FUNCS    			   	0x0705    /* Get the adapter functionality */
#define I2C_RDWR					0x0707    /* Combined R/W transfer (one stop only)*/


/* Typedefs for internal IIC connection */
typedef struct 
{
	int			fd;
	int			address;
	char		buf[4];
	char		*fileName;
}iic_s;



static iic_s iic = { -1, 0x70, {0,0,0,0}, "/dev/i2c-0" };



int8_t I2CInit( void )
{
	// Open I²C port for reading and writing
	if ((iic.fd = open(iic.fileName, O_RDWR)) < 0) {						
		return(-1);
	}
	
	// Set the port options and set the address of the device we wish to speak to
	if (ioctl(iic.fd, I2C_SLAVE_FORCE, iic.address) < 0) {					
		return(-2);
	}
	return( 0 );
}



int8_t I2CStart( void )
{
	// I²C programming - start ultrasonic
	iic.buf[0] = 0;
	//iic.buf[1] = 0x51;			// measurement of distance
	iic.buf[1] = 0x52;				// measurement of time 
	
	if ((write(iic.fd, iic.buf, 2)) != 2) {
		return(-1);
	}
	return(0);
}



bool I2CReadSensorData(char* pData)
{
	read(iic.fd, (uint8_t *)pData, 3 );
	return( (*pData < 0xFF) );
}


