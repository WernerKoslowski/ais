#include <arpa/inet.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <stdint.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <sys/time.h>

#include "rp.h"
#include "udp_server.h"
#include "iic.h"



//********************************************
// private defines
//********************************************
#define SAVE_DATA_BINARY		"/root/iic/file_%03i.bin"
#define SAVE_DATA_ASCII_RAM		"/tmp/ram/file_%03i.txt"
#define SAVE_DATA_BINARY_RAM	"/tmp/ram/file_%03i.bin"
#define SAVE_DATA_CONFIG		"/root/iic/ConfigFile.conf"
#define LOG_FILE				"/root/iic/log.txt"



//********************************************
// extern variables
//********************************************



//********************************************
// local variables
//********************************************
static FILE *LogFile = NULL;



//********************************************
// local function prototypes
//********************************************


//********************************************
// public functions
//********************************************

/* this function opens the LogFile 
 * 
 */
int OpenLogFile( void )
{
	if( LogFile == NULL )
	{
		LogFile = fopen( LOG_FILE, "a");
		return( (LogFile == NULL)?(-2):(0) );
	}
	else
	{
		return( -1 );
	}
}



int WriteLogFile( char* pChar )
{
	if( LogFile != NULL )
	{
		fprintf( LogFile, "%s\n", pChar);
		return( 0 );
	}
	else
	{
		return( -1 );
	}
}



int SaveBinaryFile( uint16_t FileId, void *p_data, uint32_t ByteCount )
{
	FILE *file = NULL;
	char file_name[40];
	
	sprintf(file_name, SAVE_DATA_BINARY, (uint16_t)(FileId));
	
	if( (file = fopen(file_name,"a")) < 0 ){
		return(-1);
	}
	
	fwrite( (uint16_t *)p_data, (size_t)(sizeof(uint8_t)), ByteCount, file);
	
	fclose(file);
	
	return(0);
}



int DeleteFiles( void )
{
	char file_name[40];
	
	for( uint16_t ix=0; ix<=999; ix++){
		sprintf(file_name, SAVE_DATA_BINARY, ix);
		if( remove( file_name ) < 0 ){
			return( (ix > 0 )?( -1 ):( 0 ) );
		}
	}
	
	return(0);
}



int SaveConfigFile(const char* ConfigFileName, float* pData, uint32_t Count )
{
	FILE *file = NULL;
	
	if( (file = fopen(ConfigFileName,"w")) < 0 ){
		return(-1);
	}
	
	do{
		fprintf( file, "%.3f\n", *pData);
		pData++;
	}while( (--Count) > 0 );
	
	fclose(file);
	
	return(0);
}



int SaveAsciiFile( uint16_t FileId, const char* pChar )
{
	FILE *file = NULL;
	char file_name[40];
	
	sprintf(file_name, SAVE_DATA_ASCII_RAM, (uint16_t)(FileId));
	
	if( (file = fopen(file_name,"a")) < 0 ){
		return(-1);
	}
	
	fprintf( file, "%s\n", pChar);
	
	fclose(file);
	
	return(0);
}



int ReadConfigFile(const char* ConfigFileName, float* pDataOut, int ParamCount )
{
	int Count = -1;
	FILE *file = NULL;
	char ReadBuffer[20];
	
	if( (file = fopen(ConfigFileName,"r")) == NULL ){
		return(-1);
	}
	
	while( fgets( ReadBuffer, (int)sizeof( ReadBuffer ), file ) != NULL )
	{
		Count++;
		if( (ParamCount--) > 0 )
		{
			sscanf( ReadBuffer, "%f", pDataOut++ );
		}
		else
		{
			break;
		}
	}
	
	return( Count );
	
}

